#!@usr/bin/python3
import sys
import os
import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
import time
import math
import re

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.graphics import renderPDF

from svglib.svglib import svg2rlg
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Image, Paragraph, Table
from reportlab.lib import colors
from reportlab.lib.units import mm, inch
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY


### Author: Rosemarie Meuer, University of Rostock
### Last Change: August 15th, 2019

### This program reads a json file containing aggregated measurement results,
### and plots these data in 3 ways: global data as stacked barchart, statistical
### distributions as barcharts and timeseries data as x-y plots
### The plots are written in temporary svg-files. The measurements are also used
### to create an overview of the hardware details and basic information about
### the job. A PDF-report is created containing an overall summary and the
### plots.

### Extending pdf-report for new data variables
### The statistical barcharts and the units of timeseries data can be modified by changing
### the lists in the configuration file ../conf/pdfreport.conf. They may also be modified by
### changing the predefined defaults in the code as described below. In this case the
### configuration file should be non-existent, so please delete it, if it exists. Note that
### the global data cannot be modified using the configuration file.
###
### Job's global data
### The job's global values on the first page are predefined. Several subroutines must
### be modified in order to include new data variables, see list here:
### Subroutines
### GetGlobalData -- Define each global value, maximum value and units
### hbar_global -- Define which values to compare in global diagram
###
### Job's node data (static)
### The job's node variables (not time dependent) are also predefined and a list
### of names and units must be modified for new variables, see list here:
### Subroutine
### GetBarVars -- Specify data names (static) and units in the json file whose statics should be
###               plotted in barcharts (min, max, ave, stddev)
###
### Job's timeseries data (dynamic)
### The program will plot all timeseries data without further modification, but
### if one wants the units to be shown, they must be defined in the subrouinte here:
### Subroutine
### GetUnits -- Specify units of time series (dynamic) data in json file


### matplotlib-routines

def GetUnits(tsconf,key):
	# Define units of variables in timeseries data
	# All timeseries data are plotted

	units = ''
	label = key
	if len(tsconf) == 0:
		# default values
		if key == 'proc_cpu_usage':
			units = '%'
		elif key == 'proc_mem_rss_sum':
			units = 'Bytes'
		elif key == 'node_cpu_usage':
			units = '%'
	else:
		# use values from configuration file
		for line in tsconf:
			type,var,unit,lab=line.split(";")
			if var == key:
				units = unit
				label = lab
				break

	return units,label

def GetBarVars(conf,agg_nodes):
	# Specify which variables in 'nodes' data to plot as barcharts and the units of these data in the databank
	NoneType=type(None)

	if len(conf) == 0:
		# default values
		print('no config file, using default')
		#default_barnames  = ['cpu_time_user','mem_rss_max','read_bytes','write_bytes','cpu_usage','read_count','write_count']
		#default_barunits  = ['s'            , 'Bytes'     , 'Bytes'    , 'Bytes'     , '%', 'Counts', 'Counts']
		#default_bc_labels = ['CPU usage','Memory MaxRSS','I/O Read Size','I/O Write Size','I/O Read Count','I/O Write Count']
		default_barnames  = ['cpu_time_user','cpu_usage','mem_rss_max','read_bytes','write_bytes','read_count','write_count']
		default_barunits  = ['s'            , '%'       ,'Bytes'     , 'Bytes'    , 'Bytes'     , '%', 'Counts', 'Counts']
		default_bc_labels = ['CPU Time User','CPU usage','Memory MaxRSS','I/O Read Size','I/O Write Size','I/O Read Count','I/O Write Count']
		barnames = []
		barunits = []
		bc_labels = []
		# check for presence of data in json
		for i in range(0,len(default_barnames)):
			var=default_barnames[i]
			unit=default_barunits[i]
			label=default_bc_labels[i]
			if var in agg_nodes[0] and type(agg_nodes[0][var]) != NoneType:
				barnames.append(var)
				barunits.append(unit)
				bc_labels.append(label)
			else:
				print('missing data: ',var)
	else:
		# use values from configuration file
		barnames = []
		barunits = []
		bc_labels = []
		for line in conf:
			if(line.startswith("bar")):
				line=line.strip()
				plottype,var,unit,label=line.split(";")
				if var in agg_nodes[0] and type(agg_nodes[0][var]) != NoneType:
					barnames.append(var)
					barunits.append(unit)
					bc_labels.append(label)
				else:
					print('Variable',var,'does not exist for first node; check configuration file')

	return barnames,barunits,bc_labels
def GetGpuVars(conf,agg_nodes):
	# Specify which variables in 'nodes' data to plot as barcharts and the units of these data in the databank
	NoneType=type(None)

	if agg_nodes[0]['gpus'] is None:
		return [], [], []

	if len(conf) == 0:
		# default values
		print('no config file, using default gpu setting')
		# here you can define default values
		default_gpunames  = []
		default_gpuunits  = []
		default_gp_labels = []
		gpunames = []
		gpuunits = []
		gp_labels = []
		# check for presence of data in json
		for i in range(0,len(default_gpunames)):
			var=default_gpunames[i]
			unit=default_gpuunits[i]
			label=default_gp_labels[i]
			if var in agg_nodes[0]['gpus'][0] and type(agg_nodes[0]['gpus'][0][var]) != NoneType:
				gpunames.append(var)
				gpuunits.append(unit)
				gp_labels.append(label)
			else:
				print('missing data: ',var)
	else:
		# use values from configuration file
		gpunames = []
		gpuunits = []
		gp_labels = []
		for line in conf:
			if(line.startswith("gpubx")):
				line=line.strip()
				plottype,var,unit,label=line.split(";")
				if var in agg_nodes[0]['gpus'][0] and type(agg_nodes[0]['gpus'][0][var]) != NoneType:
					gpunames.append(var)
					gpuunits.append(unit)
					gp_labels.append(label)
				else:
					print('Variable',var,'does not exist for first node; check configuration file')

	return gpunames,gpuunits,gp_labels

def GetGlobalData(agg_dict):
	# default values
	data = [global_time(agg_dict['job']['used_time'],agg_dict['job']['requested_time']),
	global_cpu_usage(agg_dict['nodes']), global_memory(agg_dict['nodes']),
	global_swap(agg_dict['nodes']), global_IO(agg_dict['nodes']),
	global_network(agg_dict['nodes'])]

	return data

def GetGlobalGPUData(agg_dict):
	# default values
	data = [global_time(agg_dict['job']['used_time'],agg_dict['job']['requested_time']),
	global_cpu_usage(agg_dict['nodes']), global_memory(agg_dict['nodes']),
	global_swap(agg_dict['nodes']),
	global_gpu_usage(agg_dict['nodes']), global_gpu_memory(agg_dict['nodes']),
	global_IO(agg_dict['nodes']),
	global_network(agg_dict['nodes'])]

	return data

def hbar_global(data, job_id, tmpdir):
	# Function to create stacked bar chart of the global results 'data'
	# for the identifier 'job_id'.
	# This subroutine defines specific variable pairs for comparison and
	# the corresponding units:
	# <var>_v measured global value
	# <var>_m corresponding maximum value
	# <var>_u correspoinding units
	# special case: I/O compares ratio of reads with writes, since there is no
	# applicable upper limit. Then it converts these values for use in a
	# stacked bar chart. A bar chart diagram is created and
	# written to a file in SVG-format in 'tmpdir/global_barchart_<job_id>.svg'.

	graficfile = tmpdir + '/' + 'global_barchart_' + job_id + '.svg'

	### Define specific value pairs
	# convert input formats
	walltime_v = float(data[0]['walltime_v'])   # value of walltime
	walltime_m = float(data[0]['walltime_m'])   # maximum walltime for comparison
	walltime_u = data[0]['walltime_u']          # units of walltime inputs

	cpu_usage_v = float(data[1]['cpu_usage_v'])  # value of cpu_usage
	cpu_usage_m = float(data[1]['cpu_usage_m'])  # maximum cpu_usage for comparison
	cpu_usage_u = data[1]['cpu_usage_u']         # units of cpu_usage

	memory_v = float(data[2]['memory_v'])     # value of memory
	memory_m = float(data[2]['memory_m'])     # maximum memory for comparison
	memory_u = data[2]['memory_u']            # units of memory

	swap_v = float(data[3]['swap_v'])       # value of swap
	swap_m = float(data[3]['swap_m'])      # maximum value of swap for comparison, if available, otherwise 0
	swap_u = data[3]['swap_u']             # units of swap

	IO_r = float(data[4]['IO_r'])        # value of read data (input)
	IO_w = float(data[4]['IO_w'])        # value of written data (output)
	IO_u = data[4]['IO_u']               # units of I/O data

	# Implemented for Infiniband only
	network_r = float(data[5]['network_r'])   # maximum value of network (received)
	network_x = float(data[5]['network_x'])   # maximum value of network (transmitted)
	network_u = data[5]['network_u']          # units

	# transformations and definition of plot boundaries for stacked bar chart

	walltime = (walltime_v/walltime_m * 100.)
	walltime_l = 'Walltime: ' + format(walltime_v, ".2f") + '|' + format(walltime_m, ".2f") + ' ' + walltime_u
	walltime_left = 'Walltime:'
	walltime_right = format(walltime_v, ".2f") + ' | ' + format(walltime_m, ".2f") + ' [' + walltime_u + ']'
	walltime_p = 100.

	cpu_usage = (cpu_usage_v/cpu_usage_m * 100.)
	cpu_usage_l = 'CPU-usage: ' + format(cpu_usage_v, ".2f") + '|' + format(cpu_usage_m, ".2f") + ' ' + cpu_usage_u
	cpu_usage_left = 'CPU-usage:'
	cpu_usage_right = format(cpu_usage_v, ".2f") + ' | ' + format(cpu_usage_m, ".2f") + ' [' + cpu_usage_u + ']'
	cpu_usage_p = 100.

	if abs(memory_m) < 0.001:
		memory = 0.
		memory_p = 0.
	else:
		memory = (memory_v/memory_m * 100.)
		memory_p = 100.
	memory_l = 'Memory sum: ' + format(memory_v, ".2f") + ' | ' + format(memory_m, ".2f") + ' ' + memory_u
	memory_left = 'Memory sum:'
	memory_right = format(memory_v, ".2f") + ' | ' + format(memory_m, ".2f") + ' [' + memory_u + ']'

	if swap_v > 0:
	    swap = 100.
	else:
	    swap = 0.

	swap_l = "Swap sum: " + format(swap_v, ".0f") + ' ' + swap_u
	swap_left = "Swap sum:"
	swap_right = format(swap_v, ".0f") + ' [' + swap_u + ']'
	swap_p = 100.

	IO_v = IO_r
	IO_m = IO_r + IO_w

	if abs(IO_m) < 0.001:
		IO = 0.
		IO_p = 0.
	else:
		IO = (IO_v/IO_m * 100.)
		IO_p = 100.
	IO_l = 'I/O (Read/Write): ' + format(IO_r, ".2f") + '|' + format(IO_w, ".2f") + ' ' + IO_u
	IO_left = 'I/O (Read/Write):'
	IO_right = format(IO_r, ".2f") + ' | ' + format(IO_w, ".2f") + ' [' + IO_u + ']'

	# network implemented for Infiniband only
	network_v = network_r
	network_m = network_r + network_x

	if abs(network_m) < 0.001:
		network = 0.
		network_p = 0.
	else:
		network = (network_v/network_m * 100.)
		network_p = 100.
	network_l = 'Network IB max (rcv/snd): ' + format(network_r, ".2f") + '|' + format(network_x, ".2f") + ' ' + network_u
	network_left = 'Network IB max (rcv/snd):'
	network_right = format(network_r, ".2f") + ' | ' + format(network_x, ".2f") + ' [' + network_u + ']'

	## prepare plot variables
	# axis labels
	#results = (network_l, IO_l, swap_l, memory_l, cpu_usage_l, walltime_l)
	results_left = (IO_left, network_left, swap_left, memory_left, cpu_usage_left, walltime_left)
	results_right = (IO_right, network_right, swap_right, memory_right, cpu_usage_right, walltime_right)
	# create spaced values for given interval for y-positions
	y_pos = np.arange(len(results_left))
	# data
	bottomdata = [IO, network, swap, memory, cpu_usage, walltime]
	#topdata = [IO_p, network_p, swap_p, memory_p, cpu_usage_p, walltime_p]
	# set background colors for IO_p and network_p by using the value 100.
	topdata = [100., 100., swap_p, memory_p, cpu_usage_p, walltime_p]
	#otherbottomdata = [network, IO, 0., 0., 0., 0.]
	#othertopdata = [network_p, IO_p, 0., 0., 0., 0.]
	otherbottomdata = [IO, network, 0., 0., 0., 0.]
	othertopdata = [IO_p, network_p, 0., 0., 0., 0.]
	thirdbottomdata = [0., 0., 0., 0., cpu_usage, 0.]
	thirdtopdata = [0., 0., 0., 0., cpu_usage_p, 0.]

	## create plot
	fig = plt.figure(figsize=(6,3), constrained_layout=True)

	ax = fig.add_subplot(111)
	#ax.barh(y_pos, topdata,color='lightgrey',align='center')
	#ax.barh(y_pos, bottomdata,color='orchid',align='center')
	ax.barh(y_pos, topdata,color='whitesmoke',align='center')
	ax.barh(y_pos, bottomdata,color='limegreen',align='center')
	ax.barh(y_pos, othertopdata,color='green',align='center')
	ax.barh(y_pos, otherbottomdata,color='limegreen',align='center')
	# temporäre Lösung... soll aus Recommendations abgeleitet werden
	#if cpu_usage < 20.:
	#  ax.barh(y_pos, thirdtopdata,color='whitesmoke',align='center')
	#  ax.barh(y_pos, thirdbottomdata,color='red',align='center')
	ax.set_yticks(y_pos)
	ax.set_yticklabels(results_left)

	# placing 2nd text at right is more difficult; this seems to work
	# first make twin
	ax2 = ax.twinx()
	# shut of bounding box sides
	ax2.spines["top"].set_visible(False)
	ax2.spines["right"].set_visible(False)
	ax2.spines["bottom"].set_visible(False)
	ax2.spines["left"].set_visible(False)
	# create labels
	ax2.set_yticks(np.arange(len(results_right)))
	ax2.set_yticklabels(results_right)
	# adjust spacing for labels (ticks)
	width_space=0.7
	width_bar=1
	total_space=(len(results_right)-1)*width_bar
	ax2.set_ylim((-width_space, total_space+width_space))
	#ax2.set_ylim((-0.7, 5.7))
	# shut off ticks
	ax2.tick_params(length=0)


	# remove unwanted tick parameters
	ax.tick_params(top=False, bottom=False, left=False, right=False, labelleft=True, labelbottom=False)
	# remove frame
	for spine in ax.spines.values():
	    spine.set_visible(False)

	#ax.set_xlabel('Distance')

	## create plot in window
	#plt.show()
	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0

def hbar_global_gpu(data, job_id, tmpdir):
	# Function to create stacked bar chart of the global results 'data'
	# for the identifier 'job_id'.
	# This subroutine defines specific variable pairs for comparison and
	# the corresponding units:
	# <var>_v measured global value
	# <var>_m corresponding maximum value
	# <var>_u correspoinding units
	# special case: I/O compares ratio of reads with writes, since there is no
	# applicable upper limit. Then it converts these values for use in a
	# stacked bar chart. A bar chart diagram is created and
	# written to a file in SVG-format in 'tmpdir/global_barchart_<job_id>.svg'.

	graficfile = tmpdir + '/' + 'global_barchart_' + job_id + '.svg'

	### Define specific value pairs
	# convert input formats
	walltime_v = float(data[0]['walltime_v'])   # value of walltime
	walltime_m = float(data[0]['walltime_m'])   # maximum walltime for comparison
	walltime_u = data[0]['walltime_u']          # units of walltime inputs

	cpu_usage_v = float(data[1]['cpu_usage_v'])  # value of cpu_usage
	cpu_usage_m = float(data[1]['cpu_usage_m'])  # maximum cpu_usage for comparison
	cpu_usage_u = data[1]['cpu_usage_u']         # units of cpu_usage

	memory_v = float(data[2]['memory_v'])     # value of memory
	memory_m = float(data[2]['memory_m'])     # maximum memory for comparison
	memory_u = data[2]['memory_u']            # units of memory

	swap_v = float(data[3]['swap_v'])       # value of swap
	swap_m = float(data[3]['swap_m'])      # maximum value of swap for comparison, if available, otherwise 0
	swap_u = data[3]['swap_u']             # units of swap

	gpu_usage_v = float(data[4]['gpu_usage_v'])  # value of gpu_usage
	gpu_usage_m = float(data[4]['gpu_usage_m'])  # maximum gpu_usage for comparison
	gpu_usage_u = data[4]['gpu_usage_u']         # units of gpu_usage

	gpu_memory_v = float(data[5]['gpu_memory_v'])     # value of gpu memory
	gpu_memory_m = float(data[5]['gpu_memory_m'])     # maximum gpu memory for comparison
	gpu_memory_u = data[5]['gpu_memory_u']            # units of gpu memory

	IO_r = float(data[6]['IO_r'])        # value of read data (input)
	IO_w = float(data[6]['IO_w'])        # value of written data (output)
	IO_u = data[6]['IO_u']               # units of I/O data

	# Implemented for Infiniband only
	network_r = float(data[7]['network_r'])   # maximum value of network (received)
	network_x = float(data[7]['network_x'])   # maximum value of network (transmitted)
	network_u = data[7]['network_u']          # units

	# transformations and definition of plot boundaries for stacked bar chart

	walltime = (walltime_v/walltime_m * 100.)
	walltime_l = 'Walltime: ' + format(walltime_v, ".2f") + '|' + format(walltime_m, ".2f") + ' ' + walltime_u
	walltime_left = 'Walltime:'
	walltime_right = format(walltime_v, ".2f") + ' | ' + format(walltime_m, ".2f") + ' [' + walltime_u + ']'
	walltime_p = 100.

	cpu_usage = (cpu_usage_v/cpu_usage_m * 100.)
	cpu_usage_l = 'CPU-usage: ' + format(cpu_usage_v, ".2f") + '|' + format(cpu_usage_m, ".2f") + ' ' + cpu_usage_u
	cpu_usage_left = 'CPU-usage:'
	cpu_usage_right = format(cpu_usage_v, ".2f") + ' | ' + format(cpu_usage_m, ".2f") + ' [' + cpu_usage_u + ']'
	cpu_usage_p = 100.

	if abs(memory_m) < 0.001:
		memory = 0.
		memory_p = 0.
	else:
		memory = (memory_v/memory_m * 100.)
		memory_p = 100.
	memory_l = 'Memory sum: ' + format(memory_v, ".2f") + ' | ' + format(memory_m, ".2f") + ' ' + memory_u
	memory_left = 'Memory sum:'
	memory_right = format(memory_v, ".2f") + ' | ' + format(memory_m, ".2f") + ' [' + memory_u + ']'

	if swap_v > 0:
	    swap = 100.
	else:
	    swap = 0.

	swap_l = "Swap sum: " + format(swap_v, ".0f") + ' ' + swap_u
	swap_left = "Swap sum:"
	swap_right = format(swap_v, ".0f") + ' [' + swap_u + ']'
	swap_p = 100.

	gpu_usage = (gpu_usage_v/gpu_usage_m * 100.)
	gpu_usage_l = 'GPU-usage: ' + format(gpu_usage_v, ".2f") + '|' + format(gpu_usage_m, ".2f") + ' ' + gpu_usage_u
	gpu_usage_left = 'GPU-usage:'
	gpu_usage_right = format(gpu_usage_v, ".2f") + ' | ' + format(gpu_usage_m, ".2f") + ' [' + gpu_usage_u + ']'
	gpu_usage_p = 100.

	if abs(gpu_memory_m) < 0.001:
		gpu_memory = 0.
		gpu_memory_p = 0.
	else:
		gpu_memory = (gpu_memory_v/gpu_memory_m * 100.)
		gpu_memory_p = 100.
	gpu_memory_l = 'GPU Memory sum: ' + format(gpu_memory_v, ".2f") + ' | ' + format(gpu_memory_m, ".2f") + ' ' + gpu_memory_u
	gpu_memory_left = 'GPU Memory sum:'
	gpu_memory_right = format(gpu_memory_v, ".2f") + ' | ' + format(gpu_memory_m, ".2f") + ' [' + gpu_memory_u + ']'

	IO_v = IO_r
	IO_m = IO_r + IO_w

	if abs(IO_m) < 0.001:
		IO = 0.
		IO_p = 0.
	else:
		IO = (IO_v/IO_m * 100.)
		IO_p = 100.
	IO_l = 'I/O (Read/Write): ' + format(IO_r, ".2f") + '|' + format(IO_w, ".2f") + ' ' + IO_u
	IO_left = 'I/O (Read/Write):'
	IO_right = format(IO_r, ".2f") + ' | ' + format(IO_w, ".2f") + ' [' + IO_u + ']'

	# network implemented for Infiniband only
	network_v = network_r
	network_m = network_r + network_x

	if abs(network_m) < 0.001:
		network = 0.
		network_p = 0.
	else:
		network = (network_v/network_m * 100.)
		network_p = 100.
	network_l = 'Network IB max (rcv/snd): ' + format(network_r, ".2f") + '|' + format(network_x, ".2f") + ' ' + network_u
	network_left = 'Network IB max (rcv/snd):'
	network_right = format(network_r, ".2f") + ' | ' + format(network_x, ".2f") + ' [' + network_u + ']'

	## prepare plot variables
	# axis labels
	#results = (network_l, IO_l, gpu_memory_l, gpu_usage_l, swap_l, memory_l, cpu_usage_l, walltime_l)
	results_left = (IO_left, network_left, gpu_memory_left, gpu_usage_left, swap_left, memory_left, cpu_usage_left, walltime_left)
	results_right = (IO_right, network_right, gpu_memory_right, gpu_usage_right, swap_right, memory_right, cpu_usage_right, walltime_right)
	# create spaced values for given interval for y-positions
	y_pos = np.arange(len(results_left))
	# data
	bottomdata = [IO, network, gpu_memory, gpu_usage, swap, memory, cpu_usage, walltime]
	#topdata = [IO_p, network_p, gpu_memory_p, gpu_usage_p,  swap_p, memory_p, cpu_usage_p, walltime_p]
	# set background colors for IO_p and network_p by using the value 100.
	topdata = [100., 100., gpu_memory_p, gpu_usage_p, swap_p, memory_p, cpu_usage_p, walltime_p]
	#otherbottomdata = [network, IO, 0., 0., 0., 0., 0., 0.]
	#othertopdata = [network_p, IO_p, 0., 0., 0., 0., 0., 0.]
	otherbottomdata = [IO, network, 0., 0., 0., 0., 0., 0.]
	othertopdata = [IO_p, network_p, 0., 0., 0., 0., 0., 0.]
	thirdbottomdata = [0., 0., 0., 0., 0., 0., cpu_usage, 0.]
	thirdtopdata = [0., 0., 0., 0., 0., 0., cpu_usage_p, 0.]

	## create plot
	fig = plt.figure(figsize=(6,3), constrained_layout=True)

	ax = fig.add_subplot(111)
	#ax.barh(y_pos, topdata,color='lightgrey',align='center')
	#ax.barh(y_pos, bottomdata,color='orchid',align='center')
	ax.barh(y_pos, topdata,color='whitesmoke',align='center')
	ax.barh(y_pos, bottomdata,color='limegreen',align='center')
	ax.barh(y_pos, othertopdata,color='green',align='center')
	ax.barh(y_pos, otherbottomdata,color='limegreen',align='center')
	# temporäre Lösung... soll aus Recommendations abgeleitet werden
	#if cpu_usage < 20.:
	#  ax.barh(y_pos, thirdtopdata,color='whitesmoke',align='center')
	#  ax.barh(y_pos, thirdbottomdata,color='red',align='center')
	ax.set_yticks(y_pos)
	ax.set_yticklabels(results_left)

	# placing 2nd text at right is more difficult; this seems to work
	# first make twin
	ax2 = ax.twinx()
	# shut of bounding box sides
	ax2.spines["top"].set_visible(False)
	ax2.spines["right"].set_visible(False)
	ax2.spines["bottom"].set_visible(False)
	ax2.spines["left"].set_visible(False)
	# create labels
	ax2.set_yticks(np.arange(len(results_right)))
	ax2.set_yticklabels(results_right)
	# adjust spacing for labels (ticks)
	width_space=0.7
	width_bar=1
	total_space=(len(results_right)-1)*width_bar
	ax2.set_ylim((-width_space, total_space+width_space))
	#ax2.set_ylim((-0.7, 5.7))
	# shut off ticks
	ax2.tick_params(length=0)


	# remove unwanted tick parameters
	ax.tick_params(top=False, bottom=False, left=False, right=False, labelleft=True, labelbottom=False)
	# remove frame
	for spine in ax.spines.values():
	    spine.set_visible(False)

	#ax.set_xlabel('Distance')

	## create plot in window
	#plt.show()
	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0

def boxplot(case):
	# This function creates a horizontal distribution boxplot of simple data points
	data = case['bpdata']
	graficfile = case['bpgraficfile']

	fig = plt.figure(figsize=(7,2),dpi=100)
	ax = plt.subplot(1,1,1)
	ax.boxplot(data,0, sym='rs', vert=False, whis=[0,100], positions=[0], widths=[0.5])
	ax.set_yticks([])
	ax.xaxis.set_tick_params(labelsize=22)
	#ax.spines["top"].set_visible(False)
	#ax.spines["right"].set_visible(False)
	#ax.spines["left"].set_visible(False)
	plt.tight_layout()

	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0

def hbar_var(case):
	# This function creates a simple horizontal barchart of the values,
	# labels and units in an output file passed in the dictionary 'case'.
	#units = case['units']
	bar_labels = case['bar_labels']
	data = case['data']
	graficfile = case['graficfile']
	## prepare plot variables
	# create spaced values for given interval for y-positions
	y_pos = np.arange(len(bar_labels))
	#print('y_pos',y_pos)

	## create plot
	fig = plt.figure(figsize=(4,2), constrained_layout=True)

	ax = fig.add_subplot(111)
	#ax.barh(y_pos, data,color=['red','deepskyblue','palevioletred','limegreen'],align='center', height=0.5)
	ax.barh(y_pos, data,color=['palegreen', 'green','greenyellow','limegreen'],align='center', height=0.5)

	ax.set_yticks(y_pos)
	ax.set_yticklabels(bar_labels)

	# remove unwanted tick parameters
	ax.tick_params(top=False, bottom=False, left=False, right=False, labelleft=True, labelbottom=False)
	# remove frame
	for spine in ax.spines.values():
	    spine.set_visible(False)

	#ax.set_xlabel('Values given in ' + units, fontsize=12)
	#ax.set_title(' [' + units + ']', loc='center', fontsize=14)

	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0

def timeseries_var(units, tag, pddata, graficfile):
	# This function creates a timeseries plot of all variables in the pandas
	# file 'pddata'. If the number of curves which are plotted is less than
	# or equal to 20, a legend for each curve is included in the diagram.
	# The title of the plot is given in 'tag' and the title of the vertical
	# plot axis shows the 'units'


	#pddata.set_index([0],inplace=True)

	# plot legend if not too long
	if len(pddata.columns) > 20:
	    legend=False
	else:
	    legend=True

	# plottitle = 'cpu_usage' + ' per ' + tag + ' in ' + units
	plottitle = ''

	#f = plt.figure(figsize=(10,5), constrained_layout=True)
	# plot data
	#ax2 = pddata.plot(legend=legend, title=plottitle, ax = f.gca(), x_compat=False)
	#fig, ax = plt.subplots(figsize=(10,5),constrained_layout=True)
	fig, ax = plt.subplots(figsize=(12,3.5),constrained_layout=True)
	pddata.plot(ax=ax,legend=legend)


	# plot titles
	#ax2.set_xlabel("time")
	#ax2.set_ylabel(units)
	ax.set_ylabel(units)
	plt.xlim(pddata.index.min(),pddata.index.max())

	if legend:
		# shrink current axis by 10%, so legend will fit outside graph
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])
		plt.legend(title=tag, loc='center left', bbox_to_anchor=(1.03, 0.88))

	#set ticks every week
	#ax.xaxis.set_major_locator(mdates.HourLocator(byhour=range(0,24,1)))
	#set major ticks format
	#ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y %m %d %H'))

	# set range of y-axis to at least 1%
	x1,x2,y1,y2 = plt.axis()
	if ((y2 - y1)/y2 < .01):
		y1 = y1 - 1.
		plt.axis((x1,x2,y1,y2))

	### create plot in window
	#plt.show()
	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0
def timeseries_varsp(nsubpp,dfnames, udfs, ldfs, tag, dfs, graficfile):
	nplots=nsubpp
	#nplots=len(dfnames)
	legend=True
	for i,name in enumerate(dfnames):
		if (i == 0):
			legend_labels=list(dfs[name])
		elif (len(legend_labels) < len(list(dfs[name]))):
			legend_labels=list(dfs[name])
		# plot legend if not too long
		if len(dfs[name].columns) > 100:
			legend=False

	#print(legend_labels)

	# plottitle = 'cpu_usage' + ' per ' + tag + ' in ' + units
	plottitle = ''

	f, axes = plt.subplots(nplots,sharex=True,figsize=(14,17),squeeze=True)
	f.subplots_adjust(hspace=0)
	#plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
	#plt.setp([a.get_xticklabels() for a in f.axes[:]], horizonalalignment='left')
	#plt.setp([f.axes[-1].get_xaxis()], visible=True)
	axes[-1].tick_params(axis='x', which='minor',length=0)
	axes[-1].tick_params(axis='x', which='major', labelrotation=90)
	axes[-1].set_xlabel('day-month / time',labelpad=9,fontsize='x-large')
	for i,name in enumerate(dfnames):
		#axes[i].set_prop_cycle(cycler('color',['lightgray','black']))
		#axes[i].xaxis.set_major_formatter(mdates.DateFormatter("%Y%m%d / %H:%M"))
		axes[i].xaxis.set_major_formatter(mdates.DateFormatter("%d-%m\n%H:%M"))
		dfs[name].plot(ax=axes[i],legend=False)

		# plot titles
		axes[i].set_xlabel('day-month / time',labelpad=9,fontsize='x-large')
		#ax2.set_ylabel(units)
		if udfs[name] == '':
			udfs[name] = '-'
		axes[i].set_ylabel('[' + udfs[name] + ']', fontsize='large')
		#plt.xlim(pddata.index.min(),pddata.index.max())
		axes[i].tick_params(axis='x', which='major', labelrotation=90)
		ax2=axes[i].twinx()
		ax2.set_yticks([])
		if ldfs[name] == '':
			ldfs[name] = '-'
		ax2.set_ylabel(ldfs[name], fontsize='large')


	#plt.setp([f.axes[-1].get_xaxis()], visible=True)
	if legend:
		#plt.figlegend(title=tag, loc='lower left', bbox_to_anchor=(0, 0), borderaxespad=0.)
		plt.figlegend(title=tag, loc=[0.08, 0.01], ncol=9, labels=legend_labels)

	### create plot in window
	#plt.show()
	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0

def timeseries_gpu(nsubpg,dfnames, udfs, ldfs, tag, dfs, graficfile):
	nplots=nsubpg
	#nplots=len(dfnames)
	legend=True
	for i,name in enumerate(dfnames):
		if (i == 0):
			legend_labels=list(dfs[name])
		elif (len(legend_labels) < len(list(dfs[name]))):
			legend_labels=list(dfs[name])
		# plot legend if not too long
		if len(dfs[name].columns) > 100:
			legend=False

	#print(legend_labels)

	# plottitle = 'cpu_usage' + ' per ' + tag + ' in ' + units
	plottitle = ''

	f, axes = plt.subplots(nplots,sharex=True,figsize=(14,18),squeeze=True)
	f.subplots_adjust(hspace=0)
	#plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
	#plt.setp([a.get_xticklabels() for a in f.axes[:]], horizonalalignment='left')
	#plt.setp([f.axes[-1].get_xaxis()], visible=True)
	axes[-1].tick_params(axis='x', which='minor',length=0)
	axes[-1].tick_params(axis='x', which='major', labelrotation=90)
	axes[-1].set_xlabel('day-month / time',labelpad=9,fontsize='x-large')
	for i,name in enumerate(dfnames):
		#axes[i].set_prop_cycle(cycler('color',['lightgray','black']))
		#axes[i].xaxis.set_major_formatter(mdates.DateFormatter("%Y%m%d / %H:%M"))
		axes[i].xaxis.set_major_formatter(mdates.DateFormatter("%d-%m\n%H:%M"))
		dfs[name].plot(ax=axes[i],legend=False)

		# plot titles
		axes[i].set_xlabel('day-month / time',labelpad=9,fontsize='x-large')
		#ax2.set_ylabel(units)
		if udfs[name] == '':
			udfs[name] = '-'
		axes[i].set_ylabel('[' + udfs[name] + ']', fontsize='large')
		#plt.xlim(pddata.index.min(),pddata.index.max())
		axes[i].tick_params(axis='x', which='major', labelrotation=90)
		ax2=axes[i].twinx()
		ax2.set_yticks([])
		if ldfs[name] == '':
			ldfs[name] = '-'
		ax2.set_ylabel(ldfs[name], fontsize='large')


	#plt.setp([f.axes[-1].get_xaxis()], visible=True)
	if legend:
		#plt.figlegend(title=tag, loc='lower left', bbox_to_anchor=(0, 0), borderaxespad=0.)
		plt.figlegend(title=tag, loc=[0.1, 0.04], ncol=8, labels=legend_labels)

	### create plot in window
	#plt.show()
	## write plot to file in svg format
	plt.savefig(graficfile)
	plt.close()

	return 0

def global_time(time_v_s,time_m_s):
	# Function to create dictionary of global time values for stacked bar chart
	# diagram and corresponding units

	if (time_m_s > 24 * 3600):
		time_u = 'd'
		time_v = time_v_s / 3600 / 24
		time_m = time_m_s / 3600 / 24
	elif (time_m_s > 3600):
		time_u = 'h'
		time_v = time_v_s / 3600
		time_m = time_m_s / 3600
	elif (time_m_s > 60):
		time_u = 'm'
		time_v = time_v_s / 60
		time_m = time_v_s / 60
	else:
		time_u = 's'
		time_v = time_v_s
		time_m = time_m_s

	return {
		"walltime_v" : time_v,
		"walltime_m" : time_m,
		"walltime_u" : time_u
	}

def global_cpu_usage(agg_nodes):
	# Function to create dictionary of global cpu usage values for stacked bar chart
	# diagram and corresponding units
	cpu_usage_v = 0.
	cpu_usage_m = 0.
	cpu_usage_u = '%'
	num_cores_job = 0
	for i in range(0,len(agg_nodes)):
		#num_sockets = agg_nodes[i]['sockets']
		#num_cores_per_socket = agg_nodes[i]['cores_per_socket']
		#num_cores_job = num_sockets * num_cores_per_socket + num_cores_job
		num_cores_job = agg_nodes[i]['num_cores'] + num_cores_job
		cpu_usage_v = agg_nodes[i]['cpu_usage'] + cpu_usage_v

	cpu_usage_v = cpu_usage_v / num_cores_job
	cpu_usage_m = 100.
	if (cpu_usage_v > cpu_usage_m):
		cpu_usage_m = cpu_usage_v

	return {
		"cpu_usage_v" : cpu_usage_v,
		"cpu_usage_m" : cpu_usage_m,
		"cpu_usage_u" : cpu_usage_u
		}


def global_memory(agg_nodes):
	# Function to create dictionary of global memory values for stacked bar chart
	# diagram and corresponding units

	memory_v = 0
	memory_m = 0
	for i in range(0,len(agg_nodes)):
		memory_v = agg_nodes[i]['mem_rss_max'] + memory_v
		memory_m = (agg_nodes[i]['alloc_mem'] * 1000000) + memory_m

	if (memory_m > 1.e12):
		memory_m = memory_m * 1.e-12
		memory_v = memory_v * 1.e-12
		memory_u = 'TB'
	elif (memory_m > 1.e9):
		memory_m = memory_m * 1.e-9
		memory_v = memory_v * 1.e-9
		memory_u = 'GB'
	elif (memory_m > 1.e6):
		memory_m = memory_m * 1.e-6
		memory_v = memory_v * 1.e-6
		memory_u = 'MB'
	elif (memory_m > 1.e3):
		memory_m = memory_m * 1.e-3
		memory_v = memory_v * 1.e-3
		memory_u = 'KB'
	else:
		memory_u = 'Bytes'

	return {
		"memory_v" : memory_v,
		"memory_m" : memory_m,
		"memory_u" : memory_u
		}

def global_swap(agg_nodes):
	# Function to create dictionary of global swap values for stacked bar chart
	# diagram and corresponding units
	swap_v = 0
	for i in range(0,len(agg_nodes)):
		swap_v = agg_nodes[i]['mem_swap_max'] + swap_v

	swap_m = 0
	swap_u = 'Bytes'
	return {
		"swap_v" : swap_v,
		"swap_m" : swap_m,
		"swap_u" : swap_u
		}

def global_gpu_usage(agg_nodes):
	# Function to create dictionary of global average gpu usage values for stacked bar chart
	# diagram and corresponding units
	gpu_usage_v = 0.
	gpu_usage_m = 0.
	gpu_usage_u = '%'
	ngpus=0
	for i in range(0,len(agg_nodes)):
		for ng in range(0,len(agg_nodes[i]['gpus'])):
			ngpus=ngpus+1
			gpu_usage_v = agg_nodes[i]['gpus'][ng]['usage_avg'] + gpu_usage_v

	if ngpus > 1:
		gpu_usage_v = gpu_usage_v / ngpus

	gpu_usage_m = 100.
	if (gpu_usage_v > gpu_usage_m):
		gpu_usage_m = cpu_usage_v

	return {
		"gpu_usage_v" : gpu_usage_v,
		"gpu_usage_m" : gpu_usage_m,
		"gpu_usage_u" : gpu_usage_u
		}


def global_gpu_memory(agg_nodes):
	# Function to create dictionary of global gpu memory values for stacked bar chart
	# diagram and corresponding units

	gpu_memory_v = 0
	gpu_memory_m = 0

	for i in range(0,len(agg_nodes)):
		for ng in range(0,len(agg_nodes[i]['gpus'])):
			gpu_memory_v = agg_nodes[i]['gpus'][ng]['mem_max'] + gpu_memory_v
			gpu_memory_m = agg_nodes[i]['gpus'][ng]['mem'] + gpu_memory_m

	gpu_memory_u = 'MB'
	if (gpu_memory_m > 1.e06):
		gpu_memory_m = gpu_memory_m * 1.e-06
		gpu_memory_v = gpu_memory_m * 1.e-06
		gpu_memory_u = 'TB'
	elif (gpu_memory_m > 1.e3):
		gpu_memory_m = gpu_memory_m * 1.e-03
		gpu_memory_v = gpu_memory_m * 1.e-03
		gpu_memory_u = 'GB'
	elif (gpu_memory_m > 1.):
		gpu_memory_u = 'MB'
	elif (gpu_memory_m > 1.e-3):
		gpu_memory_m = gpu_memory_m * 1.e+03
		gpu_memory_v = gpu_memory_m * 1.e+03
		gpu_memory_u = 'KB'
	else:
		gpu_memory_m = gpu_memory_m * 1.e+06
		gpu_memory_v = gpu_memory_m * 1.e+06
		gpu_memory_u = 'Bytes'

	return {
		"gpu_memory_v" : gpu_memory_v,
		"gpu_memory_m" : gpu_memory_m,
		"gpu_memory_u" : gpu_memory_u
		}

def global_IO(agg_nodes):
	# Function to create dictionary of global I/O values for stacked bar chart
	# diagram and corresponding units

	IO_r = 0
	IO_w = 0
	for i in range(0,len(agg_nodes)):
		IO_r = agg_nodes[i]['read_bytes'] + IO_r
		IO_w = agg_nodes[i]['write_bytes'] + IO_w
	IO_max = max(IO_r,IO_w)
	if (IO_max > 1.e12):
		IO_r = IO_r * 1.e-12
		IO_w = IO_w * 1.e-12
		IO_u = 'TB'
	elif (IO_max > 1.e9):
		IO_r = IO_r * 1.e-9
		IO_w = IO_w * 1.e-9
		IO_u = 'GB'
	elif (IO_max > 1.e6):
		IO_r = IO_r * 1.e-6
		IO_w = IO_w * 1.e-6
		IO_u = 'MB'
	elif (IO_max > 1.e3):
		IO_r = IO_r * 1.e-3
		IO_w = IO_w * 1.e-3
		IO_u = 'KB'
	else:
		IO_u = 'Bytes'
	return {
		"IO_r" : IO_r,
		"IO_w" : IO_w,
		"IO_u" : IO_u
		}


def global_network(agg_nodes):
	# Dummy function to create dictionary of global network values for stacked bar chart
	# diagram and corresponding units.
	# To be extended when test data are available!
	#network_v = 0
	#network_m = 80
	#network_u = 'Gb/s'
	NoneType=type(None)
	network_r = 0
	network_x = 0
	for i in range(0,len(agg_nodes)):
		if 'ib_rcv_max' in agg_nodes[i] and (type(agg_nodes[i]['ib_rcv_max']) != NoneType):
			#print(agg_nodes[i]['ib_rcv_max'])
			#if (type(agg_nodes[i]['ib_rcv_max']) == NoneType):
			network_r = max(agg_nodes[i]['ib_rcv_max'],network_r)
		if 'ib_xmit_max' in agg_nodes[i] and (type(agg_nodes[i]['ib_xmit_max']) != NoneType):
			network_x = max(agg_nodes[i]['ib_xmit_max'],network_x)
	ib_max = max(network_r,network_x)
	if (ib_max > 1.e12):
		network_r = network_r * 1.e-12
		network_x = network_x * 1.e-12
		network_u = 'TB/s'
	elif (ib_max > 1.e9):
		network_r = network_r * 1.e-9
		network_x = network_x * 1.e-9
		network_u = 'GB/s'
	elif (ib_max > 1.e6):
		network_r = network_r * 1.e-6
		network_x = network_x * 1.e-6
		network_u = 'MB/s'
	elif (ib_max > 1.e3):
		network_r = network_r * 1.e-3
		network_x = network_x * 1.e-3
		network_u = 'KB/s'
	else:
		network_u = 'Bytes/s'
	return {
		"network_r" : network_r,
		"network_x" : network_x,
		"network_u" : network_u
		}

def node_statistics(name, label, job_id, agg_nodes, units, tmpdir):
	# Function to create a dictionary needed to creat a barchart containing
	# statistics of the aggregated node values
	# for the measurement 'name' in 'agg_nodes'
	fileid = 'bar_' + name
	bpfileid = 'bp_' + name
	#graficfile = tmpdir + '/' + 'bar_' + name + '_' + str(job_id) + '.svg'
	graficfile = tmpdir + '/' + fileid + '_' + str(job_id) + '.svg'
	bpgraficfile = tmpdir + '/' + bpfileid + '_' + str(job_id) + '.svg'
	#names = [name + '_min', name + '_max', name + '_ave']
	#names = ['Min', 'Max', 'Avg', 'StdDev']
	names = ['Min', 'Max', 'StdDev', 'Avg']
	values = []
	for j in range(0,len(agg_nodes)):
		val = agg_nodes[j][name]
		if (name == 'cpu_usage'):
			#print('cpu_usage, num_cores',val,agg_nodes[j]['num_cores'])
			val = val / agg_nodes[j]['num_cores']

		values.append(val)

	# convert units to nice Values
	svalues = pd.Series(values)
	val_max = svalues.max()
	if (units == 's'):
		if (val_max > 24 * 3600):
			units = 'd'
			svalues = svalues / (3600 * 24)
		elif (val_max > 3600):
			units = 'h'
			svalues = svalues / 3600
		elif (val_max > 60):
			units = 'm'
			svalues = svalues / 60
	elif (units == 'Bytes'):
		if (val_max > 1.e12):
			svalues = svalues * 1.e-12
			units = 'TB'
		elif (val_max > 1.e9):
			svalues = svalues * 1.e-9
			units = 'GB'
		elif (val_max > 1.e6):
			svalues = svalues * 1.e-6
			units = 'MB'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			units = 'KB'
	elif (units == 'MB'):
		if (val_max > 1.e06):
			svalues = svalues * 1.e-06
			units = 'TB'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			units = 'GB'
		elif (val_max > 1.):
			#svalues = svalues * 1.
			units = 'MB'
		elif (val_max > 1.e-3):
			svalues = svalues * 1.e+3
			units = 'KB'
		else:
			svalues = svalues * 1.e+6
			units = 'Bytes'
	elif (units == 'Bytes/s'):
		if (val_max > 1.e12):
			svalues = svalues * 1.e-12
			units = 'TB/s'
		elif (val_max > 1.e9):
			svalues = svalues * 1.e-9
			units = 'GB/s'
		elif (val_max > 1.e6):
			svalues = svalues * 1.e-6
			units = 'MB/s'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			units = 'KB/s'
	elif (units == 'Counts'):
		if (val_max > 1.e12):
			svalues = svalues * 1.e-12
			#units = 'Counts/1.E12'
			units = '*1.E-12'
		elif (val_max > 1.e9):
			svalues = svalues * 1.e-9
			#units = 'Counts/1.E09'
			units = '*1.E-09'
		elif (val_max > 1.e6):
			svalues = svalues * 1.e-6
			#units = 'Counts/1.E06'
			units = '*1.E-06'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			#units = 'Counts/1.E03'
			units = '*1.E-03'

	val_min = svalues.min()
	val_max = svalues.max()
	val_ave = svalues.mean()
	val_std = svalues.std()
	#values = [val_min, val_max, val_ave, val_std]
	values = [val_min, val_max, val_std, val_ave]
	bar_labels = []

	for i in range(0,len(names)):
		if (name == 'cpu_usage'):
			bar_labels.append(names[i] + ': ' + format(values[i], ".1f"))
		#elif (units == 'Counts'):
		#	bar_labels.append(names[i] + ': ' + format(values[i], ".4g"))
		else:
			bar_labels.append(names[i] + ': ' + format(values[i], ".1f"))

	return {
		"variable" : name,
		"names" : names,
		"data" : values,
		"bpdata" : svalues.tolist(),
		"units" : units,
		"bar_labels" : bar_labels,
		"label" : label,
		"fileid" : fileid,
		"bpfileid" : bpfileid,
		"graficfile" : graficfile,
		"bpgraficfile" : bpgraficfile
	}

def gpu_statistics(name, label, job_id, agg_nodes, units, tmpdir):
	# Function to create a dictionary needed to creat a barchart containing
	# statistics of the aggregated node values
	# for the measurement 'name' in 'agg_nodes'
	bpfileid = 'gpu_' + name
	bpgraficfile = tmpdir + '/' + bpfileid + '_' + str(job_id) + '.svg'
	names = ['Min', 'Max', 'StdDev', 'Avg']
	values = []
	for j in range(0,len(agg_nodes)):
		for k in range(0,len(agg_nodes[j]['gpus'])):
			val = agg_nodes[j]['gpus'][k][name]

			values.append(val)

	# convert units to nice Values
	svalues = pd.Series(values)
	val_max = svalues.max()
	if (units == 's'):
		if (val_max > 24 * 3600):
			units = 'd'
			svalues = svalues / (3600 * 24)
		elif (val_max > 3600):
			units = 'h'
			svalues = svalues / 3600
		elif (val_max > 60):
			units = 'm'
			svalues = svalues / 60
	elif (units == 'Bytes'):
		if (val_max > 1.e12):
			svalues = svalues * 1.e-12
			units = 'TB'
		elif (val_max > 1.e9):
			svalues = svalues * 1.e-9
			units = 'GB'
		elif (val_max > 1.e6):
			svalues = svalues * 1.e-6
			units = 'MB'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			units = 'KB'
	elif (units == 'MB'):
		if (val_max > 1.e06):
			svalues = svalues * 1.e-06
			units = 'TB'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			units = 'GB'
		elif (val_max > 1.):
			#svalues = svalues * 1.
			units = 'MB'
		elif (val_max > 1.e-3):
			svalues = svalues * 1.e+3
			units = 'KB'
		else:
			svalues = svalues * 1.e+6
			units = 'Bytes'
	elif (units == 'Bytes/s'):
		if (val_max > 1.e12):
			svalues = svalues * 1.e-12
			units = 'TB/s'
		elif (val_max > 1.e9):
			svalues = svalues * 1.e-9
			units = 'GB/s'
		elif (val_max > 1.e6):
			svalues = svalues * 1.e-6
			units = 'MB/s'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			units = 'KB/s'
	elif (units == 'Counts'):
		if (val_max > 1.e12):
			svalues = svalues * 1.e-12
			#units = 'Counts/1.E12'
			units = '*1.E-12'
		elif (val_max > 1.e9):
			svalues = svalues * 1.e-9
			#units = 'Counts/1.E09'
			units = '*1.E-09'
		elif (val_max > 1.e6):
			svalues = svalues * 1.e-6
			#units = 'Counts/1.E06'
			units = '*1.E-06'
		elif (val_max > 1.e3):
			svalues = svalues * 1.e-3
			#units = 'Counts/1.E03'
			units = '*1.E-03'

	val_min = svalues.min()
	val_max = svalues.max()
	val_ave = svalues.mean()
	val_std = svalues.std()
	values = [val_min, val_max, val_std, val_ave]

	return {
		"variable" : name,
		"names" : names,
		"data" : values,
		"bpdata" : svalues.tolist(),
		"units" : units,
		"label" : label,
		"bpfileid" : bpfileid,
		"bpgraficfile" : bpgraficfile,
	}


def GetDynamicDataArray(agg_nodes,name):
	# Function which extracts timeseries data of measurement 'name' from
	# 'agg_nodes' and returns them in an array 't', the corresponding node
	# names are rturned in a list named 'columns' and factors for conversion
	# of the data are returned in list 'conversion'

	columns = []
	conversion = []
	for i in range(0,len(agg_nodes)):
		columns.append(agg_nodes[i]['node_name'])
		num_cores = agg_nodes[i]['num_cores']
		# special case for proc_cpu_usage, see also subroutine SpecialConversion
		if (name == 'proc_cpu_usage') and (num_cores > 1):
			conversion.append(1./num_cores)
		else:
			conversion.append(1.)

		if(i == 0):
			a=np.array([agg_nodes[i]['dynamic'][name]['data']])
		else:
			a=np.concatenate((a,[agg_nodes[i]['dynamic'][name]['data']]),axis=0)
	t=a.transpose()
	return t,columns,conversion

def GetDynamicGpuDataArray(agg_nodes,name):
	# Function which extracts timeseries gpu data of measurement 'name' from
	# 'agg_nodes' and returns them in an array 't', the corresponding node
	# names and gpu-bus are returned in a list named 'columns' and factors for conversion
	# of the data are returned in list 'conversion'

	columns = []
	conversion = []
	for i in range(0,len(agg_nodes)):
		for ng in range(0,len(agg_nodes[i]['gpus'])):
			lname=agg_nodes[i]['node_name'] + '-' + agg_nodes[i]['gpus'][ng]['bus']
			columns.append(lname)
			conversion.append(1.)

			if (i == 0) and (ng == 0):
				a=np.array([agg_nodes[i]['gpus'][ng]['dynamic'][name]['data']])
			else:
				a=np.concatenate((a,[agg_nodes[i]['gpus'][ng]['dynamic'][name]['data']]),axis=0)
	t=a.transpose()
	return t,columns,conversion

def SpecialConversion(pddata,conversion):
	# Function applies conversion factor to pandas data
	for i in range(0,len(conversion)):
		pddata[pddata.columns[i]] *= conversion[i]
	return pddata

def ConvertUnitsBytes(val,units):
	# Function to convert memory measurements into human readable units
	if (val > 1.e12):
		val = val*1.e-12
		units = 'TB'
	elif (val > 1.e9):
		val = val*1.e-9
		units = 'GB'
	elif (val > 1.e6):
		val = val*1.e-6
		units = 'MB'
	elif (val > 1.e3):
		val = val*1.e-3
		units = 'KB'
	return val, units

def ConvertUnitsDF (df,units):
	# Function to convert measurements of entire dataframe
	# into human readable units based on maximum value

	# determine maximum value of all columns of dataframe
	value = df.max().max()

	if (units == 's'):
		if (value > 24 * 3600):
			units = 'd'
			fak=1./(3600*24)
			df = df.multiply(fak,axis='columns')
		elif (value > 3600):
			units = 'h'
			fak=1./(3600)
			df = df.multiply(fak,axis='columns')
		elif (value > 60):
			units = 'm'
			fak=1./60
			df = df.multiply(fak,axis='columns')
	elif (units == 'Bytes'):
		if (value > 1.e12):
			df = df.multiply(1.e-12,axis='columns')
			units = 'TB'
		elif (value > 1.e9):
			df = df.multiply(1.e-9,axis='columns')
			units = 'GB'
		elif (value > 1.e6):
			df = df.multiply(1.e-6,axis='columns')
			units = 'MB'
		elif (value > 1.e3):
			df = df.multiply(1.e-3,axis='columns')
			units = 'KB'
	elif (units == 'MB'):
		if (value > 1.e06):
			df = df.multiply(1.e-06,axis='columns')
			units = 'TB'
		elif (value > 1.e3):
			df = df.multiply(1.e-03,axis='columns')
			units = 'GB'
		elif (value > 1.):
			units = 'MB'
		elif (value > 1.e-3):
			df = df.multiply(1.e+03,axis='columns')
			units = 'KB'
		else:
			df = df.multiply(1.e+06,axis='columns')
			units = 'Bytes'
	elif (units == 'Bytes/s'):
		if (value > 1.e12):
			df = df.multiply(1.e-12,axis='columns')
			units = 'TB/s'
		elif (value > 1.e9):
			df = df.multiply(1.e-9,axis='columns')
			units = 'GB/s'
		elif (value > 1.e6):
			df = df.multiply(1.e-6,axis='columns')
			units = 'MB/s'
		elif (value > 1.e3):
			df = df.multiply(1.e-3,axis='columns')
			units = 'KB/s'
	elif (units == 'Counts'):
		if (value > 1.e12):
			df = df.multiply(1.e-12,axis='columns')
			units = 'Counts/1.E12'
		elif (value > 1.e9):
			df = df.multiply(1.e-9,axis='columns')
			units = 'Counts/1.E09'
		elif (value > 1.e6):
			df = df.multiply(1.e-6,axis='columns')
			units = 'Counts/1.E06'
		elif (value > 1.e3):
			df = df.multiply(1.e-3,axis='columns')
			units = 'Counts/1.E03'

	return df,units

def ConvertUnitsTime(value,units):
	# Function to convert seconds to human readable units
	if (value > 24 * 3600):
			units = 'd'
			fak=1./(3600*24)
			value = value * fak
	elif (value > 3600):
			units = 'h'
			fak=1./(3600)
			value = value * fak
	elif (value > 60):
			units = 'm'
			fak=1./60
			value = value * fak

	return value, units

### report-lab-routines

def scale(drawing, scaling_factor):
	"""
	Scale a reportlab.graphics.shapes.Drawing()
	object while maintaining the aspect ratio
	"""
	scaling_x = scaling_factor
	scaling_y = scaling_factor
	drawing.width = drawing.minWidth() * scaling_x
	drawing.height = drawing.height * scaling_y
	drawing.scale(scaling_x, scaling_y)
	return drawing

def add_image(my_canvas, image_path, scaling_factor, iposx, iposy, tposx, tposy, text):
	drawing = svg2rlg(image_path)
	scaled_drawing = scale(drawing, scaling_factor=scaling_factor)
	renderPDF.draw(scaled_drawing, my_canvas, iposx, iposy)
	#my_canvas.drawString(50, 30, 'My SVG Image')
	my_canvas.drawString(tposx, tposy, text)
	#my_canvas.save()

def add_image_units(my_canvas, image_path, scaling_factor, iposx, iposy, tposx, tposy, text, units):
	drawing = svg2rlg(image_path)
	#print('iposx, iposy',iposx,iposy)
	#print('scaling_factor',scaling_factor)
	#print('scaling factor',scaling_factor)
	scaled_drawing = scale(drawing, scaling_factor=scaling_factor)
	#print('scaled_drawing',scaled_drawing)
	renderPDF.draw(scaled_drawing, my_canvas, iposx, iposy)
	#my_canvas.drawString(50, 30, 'My SVG Image')
	text = text + ' [' + units + ']'
	my_canvas.drawString(tposx, tposy, text)
	#my_canvas.save()

def add_image_only(my_canvas, image_path, scaling_factor, iposx, iposy):
	drawing = svg2rlg(image_path)
	scaled_drawing = scale(drawing, scaling_factor=scaling_factor)
	renderPDF.draw(scaled_drawing, my_canvas, iposx, iposy)
	#my_canvas.drawString(50, 30, 'My SVG Image')
	#my_canvas.drawString(tposx, tposy, text)
	#my_canvas.save()

def add_text(my_canvas, tposx, tposy, text):
	#drawing = svg2rlg(image_path)
	#scaled_drawing = scale(drawing, scaling_factor=scaling_factor)
	#renderPDF.draw(scaled_drawing, my_canvas, iposx, iposy)
	#my_canvas.drawString(50, 30, 'My SVG Image')
	#my_canvas.saveState()
	#my_canvas.rotate(90)
	my_canvas.drawString(tposx, tposy, text)
	#my_canvas.restoreState()
	#my_canvas.save()

def get_raw_report(agg_dict):
	# Function to retreave raw data as dictionary for report listing

	# check for existence of recommendations in (json) data dictionary
	if 'recommendations' in agg_dict.keys():
		recommendations = agg_dict['recommendations']
	else:
		recommendations = ['No problems detected.', 'Good work!!!']

	return {
		"requested_time" : agg_dict['job']['requested_time'],
		"start_time" : agg_dict['job']['start_time'],
		"user_name" : agg_dict['job']['user_name'],
		"num_nodes" : agg_dict['job']['num_nodes'],
		"used_queue" : agg_dict['job']['used_queue'],
		"job_id" : agg_dict['job']['job_id'],
		"used_time" : agg_dict['job']['used_time'],
		"submit_time" : agg_dict['job']['submit_time'],
		"requested_cu" : agg_dict['job']['requested_cu'],
		"end_time" : agg_dict['job']['end_time'],
		"recommendations" : recommendations
	}

def ShortenLines(lines,lmax):
	shortlines = []
	for line in lines:
		if len(line) <= lmax:
			shortlines.append(line)
		else:
			print('recommendation line too long; wrapping lines')
			l0 = 0
			ll = lmax
			if ll > len(line):
				ll=len(line)
			while l0 < len(line):
				shortlines.append(line[l0:ll])
				l0=ll
				ll=ll+lmax
				if ll >= len(line):
					ll=len(line)
	return shortlines

def ShortenLinesWords(lines,lmax):

	shortlines = []
	for line in lines:
		if len(line) <= lmax:
			shortlines.append(line)
		else:
			print('recommendation line too long; reducing number of words per line')
			words=line.split(' ')
			newline=''
			for word in words:
				if newline == '':
					newline = word
				elif len(newline)+len(word) < lmax:
					newline = newline + ' ' + word
				else:
					shortlines.append(newline)
					newline=''
			if newline != '':
				shortlines.append(newline)

	return shortlines


def get_report(agg_dict):
	# Function to create dictionary of formatted report listing
	# get raw data
	raw_report = get_raw_report(agg_dict)
	# time formats
	submit_date = time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(raw_report['submit_time']))
	start_date = time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(raw_report['start_time']))
	end_date = time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(raw_report['end_time']))
	requested_time,requested_time_units = ConvertUnitsTime(raw_report['requested_time'],'s')
	used_time, used_time_units = ConvertUnitsTime(raw_report['used_time'],'s')
	recommendations = raw_report['recommendations']
	#recommendations = ShortenLines(raw_report['recommendations'],95)
	recommendations = ShortenLinesWords(raw_report['recommendations'],90)
	# dummy values; code must be modified when aggregater delivers recommendations
	# recommendations = ['No problems detected.', 'Good work!!!']
	# recommendations contains list of strings, with maximum string length of 98 characters
	# which will fit on one line in the document (DINA4); The list itself can have any
	# number of elements (lines)

	# interpret and compact data for report
	num_nodes=agg_dict['job']['num_nodes']
	models = []
	nsockets = []
	ncores_per_socket = []
	nthreads_per_core = []
	main_memory_mean = 0
	for i in range(0,num_nodes):
		main_memory_mean = agg_dict['nodes'][i]['available_main_mem'] + main_memory_mean
		model = agg_dict['nodes'][i]['cpu_model']
		nsocket = agg_dict['nodes'][i]['sockets']
		ncores = agg_dict['nodes'][i]['cores_per_socket']
		nthreads = agg_dict['nodes'][i]['phys_threads_per_core']
		if model not in models:
			models.append(model)
		if nsocket not in nsockets:
			nsockets.append(nsocket)
		if ncores not in ncores_per_socket:
			ncores_per_socket.append(ncores)
		if nthreads not in nthreads_per_core:
			nthreads_per_core.append(nthreads)
	# Convert data
	main_memory_mean, units = ConvertUnitsBytes(main_memory_mean/num_nodes, 'Bytes')
	main_memory_mean = format(main_memory_mean, ".0f") + ' ' +  units
	# Summarize hardware
	if (len(models) > 1):
		model = 'Heterogeneous: ' + str(len(models)) + ' models'
	else:
		model = str(model)
	if (len(nsockets) > 1):
		#nsocket = "Heterogeneous"
		nsocket = 'Between ' + str(min(nsockets)) + ' and ' + str(max(nsockets))
	else:
		nsocket = str(nsocket)
	if (len(ncores_per_socket) > 1):
		#ncores = "Heterogeneous"
		ncores = 'Between ' + str(min(ncores_per_socket)) + ' and ' + str(max(ncores_per_socket))
	else:
		ncores = str(ncores)
	if (len(nthreads_per_core) > 1):
		#nthreads = "Heterogeneous"
		nthreads = 'Between ' + str(min(nthreads_per_core)) + ' and ' + str(max(nthreads_per_core))
	else:
		nthreads = str(nthreads)

	nodes = []
	for i in range(0,num_nodes):
		nodes.append(agg_dict['nodes'][i]['node_name'])

	return {
		"Requested time" : format(requested_time, ".2f") + ' ' + requested_time_units,
		"Time of job submition" : submit_date,
		"Time of job start" : start_date,
		"User name" : raw_report['user_name'],
		"Number of nodes" : raw_report['num_nodes'],
		"Queue" : raw_report['used_queue'],
		"Job-ID" : raw_report['job_id'],
		"Used time" : format(used_time, ".2f") + ' ' + used_time_units,
		"Time of submition" : submit_date,
		"Requested cores" : raw_report['requested_cu'],
		"Time of completion" : end_date,
		"CPU model" : model,
		"Memory per node" : main_memory_mean,
		"Sockets per node" : nsocket,
		"Cores per socket" : ncores,
		"Threads per core" : nthreads,
		"Node list"        : nodes,
		"Recommendations"  : recommendations
	}

def get_node_info(agg_dict):
	num_nodes=agg_dict['job']['num_nodes']
	models = []
	nsockets = []
	ncores_per_socket = []
	nthreads_per_core = []
	main_memory_mean = 0
	for i in range(0,num_nodes):
		main_memory_mean = agg_dict['nodes'][i]['available_main_mem'] + main_memory_mean
		model = agg_dict['nodes'][i]['cpu_model']
		nsocket = agg_dict['nodes'][i]['sockets']
		ncores = agg_dict['nodes'][i]['cores_per_socket']
		nthreads = agg_dict['nodes'][i]['phys_threads_per_core']
		if model not in models:
			models.append(model)
		if nsocket not in nsockets:
			nsockets.append(nsocket)
		if ncores not in ncores_per_socket:
			ncores_per_socket.append(ncores)
		if nthreads not in nthreads_per_core:
			nthreads_per_core.append(nthreads)
	main_memory_mean = main_memory_mean/num_nodes

	return {
		"CPU model" : models,
		"Mean main memory per node" : main_memory_mean,
		"Sockets per node" : nsockets,
		"Cores per socket" : ncores_per_socket,
		"Threads per core" : nthreads_per_core
	}


def pfitPageHeader(c, date, jobid, logo_path):
	c.setFont('Helvetica', 8, leading=None)
	c.drawString(50,800,'ProfiT-HPC Report')
	c.drawString(50,790,'Report for JobID: ' + str(jobid))
	c.drawString(50,780,'Time of generation: ' + date)
	scaling_factor = 0.15
	add_image(c,logo_path, scaling_factor, 450, 780, 450, 780, '')
	c.line(50,775,550,775)
def pfitPageFooter(c, page_number, max_page_number):
	c.line(50,30,550,30)
	c.setFont('Helvetica', 8, leading=None)
	#utext = 'Gefördert durch die Deutsche Forschungsgemeinschaft (DFG)'
	#c.drawString(50, 20, utext.encode("utf8") )
	c.drawString(50, 20, 'Gefördert durch die Deutsche Forschungsgemeinschaft (DFG)')
	c.drawString(50, 10, 'KO 3394/14-1, OL 241/3-1, RE 1389/9-1, VO 1262/1-1, YA 191/10-1')
	c.setFont('Helvetica', 10, leading=None)
	c.drawString(535, 20, str(page_number) + '/' + str(max_page_number))

def count_pages(rec_text,nrecpp,bc_labels,nbcpp,ts_labels,ntspp,gts_labels,ngtspp):
	nptext=0
	if(len(rec_text)>6):
		nptext=math.ceil(len(rec_text)/nrecpp)
	if nbcpp > 0:
		npbc=math.ceil(len(bc_labels)/nbcpp)
	else:
		npbc=0
	npbp=1
	npts=math.ceil(len(ts_labels)/ntspp)
	if (ngtspp > 0):
		npgs=math.ceil(len(gts_labels)/ngtspp)
	else:
		npgs=0
	#print('bc_labels')
	#print(nbcpp)
	#print(len(bc_labels))
	#print('npbc',npbc)
	return nptext,npbc,npbp,npts,npgs

######


def nodeparts(nodes):
	# determine node name prefix and node number
	# find leading characters
	pattern = re.compile("[^0-9]*")

	dictnode={}

	for node in nodes:
		if pattern.match(node):
			p=pattern.match(node)
			span=p.span()
			if (isInt(node[span[1]:])):
				nodepre=node[span[0]:span[1]]
				nodenum=node[span[1]:]
				if nodepre in dictnode:
					dictnode[nodepre].append(nodenum)
				else:
					dictnode.update({nodepre:[nodenum]})
			else:
				if node in dictnode:
					dictnode[node].append('')
				else:
					dictnode.update({node:['']})

		else:
			if node in dictnode:
				dictnode[node].append('')
			else:
				dictnode.update({node:['']})

	return dictnode

def isInt(astring):
	""" Is the given string an integer? """
	try: int(astring)
	except ValueError: return 0
	else: return 1

def compact(cmax,key,numbers):
	# cmax is the maximum number of characters allowed in the resulting string
	# for longer strings, the separator ; is used
	cm=cmax
	if len(numbers) > 1:
		numbers.sort()
		numlist=str(numbers[0])
		numlast=int(numbers[0])
		cont=''
		for n in range(1,len(numbers)):
			num=numbers[n]
			if (int(num) == numlast + 1):
				cont='-' + num
			else:
				if (len(key)+len(numlist)+len(num)+4 > cm):
					numlist = numlist + cont + ',;' + num
					cm = cm + cmax
				else:
					numlist = numlist + cont + ',' + num
				cont = ''
			numlast=int(num)
		numlist = numlist + cont

	elif (len(numbers) == 1):
		numlist=str(numbers[0])
	else:
		numlist=''

	return numlist

def NodeCompactList(cmax,nodes):
	if len(nodes) <= 1:
		return nodes

	dictnode=nodeparts(nodes)
	compact_list=[]
	for key in dictnode:
		numlist=compact(cmax,key,dictnode[key])
		if numlist != '':
			#compact_list.append(key + '[' + numlist + ']')
			lines=numlist.split(';')
			for line in lines:
				compact_list.append(key + '[' + line.rstrip(',') + ']')
		else:
			compact_list.append(key)


	return compact_list



##### xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
class ReportMaker(object):
	#
	#----------------------------------------------------------------------
	def __init__(self, report,tmpdir,pdfdir,bc_units,bc_labels,bc_files,bp_files,bp_cases,ts_labels,ts_files,nsubpp,gts_labels,gts_files,nsubpg,gp_units,gp_labels,gp_files,gp_cases):
		pdfreportDirectory = os.path.dirname(os.path.realpath(__file__))
		pdfreportsrc = pdfreportDirectory + '/../src'
		self.logo_path = pdfreportsrc + '/logo.svg'
		self.report = report
		self.tmpdir = tmpdir
		self.pdfdir = pdfdir
		self.bc_units = bc_units
		self.bc_labels = bc_labels
		self.bc_files = bc_files
		self.bp_files = bp_files
		self.bp_cases = bp_cases
		self.gp_units = gp_units
		self.gp_labels = gp_labels
		self.gp_files = gp_files
		self.gp_cases = gp_cases
		self.ts_labels = ts_labels
		self.ts_files = ts_files
		self.nsubpp = nsubpp
		self.gts_labels = gts_labels
		self.gts_files = gts_files
		self.nsubpg = nsubpg
		jobid = report['Job-ID']
		pdffile = pdfdir + '/' + 'pdf_report_' + str(jobid) + '.pdf'
		#c = canvas.Canvas(pdffile, pagesize=A4)
		#date = time.strftime('%d/%m/%Y %H:%M:%S')
		self.c = canvas.Canvas(pdffile, pagesize=A4)
		self.styles = getSampleStyleSheet()
		self.width, self.height = A4
	#----------------------------------------------------------------------
	def createParagraph(self, ptext, x, y, style=None):
		""""""
		if not style:
			style = self.styles["Normal"]
		p = Paragraph(ptext, style=style)
		p.wrapOn(self.c, self.width, self.height)
		p.drawOn(self.c, *self.coord(x, y, mm))

	#----------------------------------------------------------------------
	def coord(self, x, y, unit=1):
		""""""
		x, y = x * unit, self.height -  y * unit
		return x, y
	def savePDF(self):
		""""""
		self.c.save()
		#def showPage(self):
		#    """"""
		#    self.c.showPage()
	#def make_formatted_report(self,report,tmpdir,pdfdir,bc_units,bc_labels,bc_files,ts_labels,ts_files,nsubpp):
	def make_formatted_report(self):
		#pdfreportDirectory = os.path.dirname(os.path.realpath(__file__))
		#pdfreportsrc = pdfreportDirectory + '/src'
		#logo_path = pdfreportsrc + '/logo.svg'
		logo_path = self.logo_path
		report = self.report
		tmpdir = self.tmpdir
		pdfdir = self.pdfdir
		bc_units = self.bc_units
		bc_labels = self.bc_labels
		bc_files = self.bc_files
		bp_files = self.bp_files
		bp_cases = self.bp_cases
		gp_units = self.gp_units
		gp_labels = self.gp_labels
		gp_files = self.gp_files
		gp_cases = self.gp_cases
		ts_labels = self.ts_labels
		ts_files = self.ts_files
		nsubpp = self.nsubpp
		gts_labels = self.gts_labels
		gts_files = self.gts_files
		nsubpg = self.nsubpg
		jobid = report['Job-ID']

		#pdffile = pdfdir + '/' + 'pdf_report_' + str(jobid) + '.pdf'
		#c = canvas.Canvas(pdffile, pagesize=A4)
		date = time.strftime('%d/%m/%Y %H:%M:%S')

		page_number = 1
		# recommendations text, stored as list items
		rec_text=report['Recommendations']
		# number of recommendations per page
		nrecpp=32
		# number of bar charts per page
		#nbcpp=len(bc_labels)
		#zero shuts off page
		#nbcpp=14
		nbcpp=0
		# number of plots per page
		# nsubpp
		nptext,npbc,npbp,npts,npgs = count_pages(rec_text,nrecpp,bc_labels,nbcpp,ts_labels,nsubpp,gts_labels,nsubpg)

		###########################################################################################################
		# Erste Seite
		# create header
		pfitPageHeader(self.c, date, jobid, logo_path)

		# Title
		self.c.setFont('Helvetica',20,leading=None)
		self.c.drawCentredString(300, 740, 'ProfiT-HPC Report')   # center text around this point (x,y from bottom)


		# page content
		self.c.setFont('Helvetica',14,leading=None)
		x = 100
		xt = 210
		xn = 350
		y = 700
		self.c.drawString(x - 20, y, 'Job Overview:')   # text at this point (x,y from bottom)
		self.c.drawString(xn - 20, y, 'Node Information:')   # text at this point (x,y from bottom)
		self.c.setFont('Helvetica', 10, leading=None)

		y = y - 30
		space = -12

		self.c.drawString(x, y, 'Job-ID: ')
		self.c.drawString(xt, y, str(jobid))
		self.c.drawString(xn, y, 'CPU model: ')
		y += space
		self.c.drawString(x, y, 'User name: ')
		self.c.drawString(xt, y, report['User name'])
		self.c.drawString(xn, y, report['CPU model'])
		y += space
		self.c.drawString(x, y, 'Queue: ')
		self.c.drawString(xt, y, str(report['Queue']))
		self.c.drawString(xn, y, 'Memory per node: ' + report['Memory per node'])
		y += space
		self.c.drawString(x, y, 'Number of nodes: ')
		self.c.drawString(xt, y, str(report['Number of nodes']))
		self.c.drawString(xn, y, 'Sockets per node: ' + report['Sockets per node'])
		y += space
		self.c.drawString(x, y, 'Requested cores: ')
		self.c.drawString(xt, y, str(report['Requested cores']))
		self.c.drawString(xn, y, 'Cores per socket: ' + report['Cores per socket'])
		y += space
		self.c.drawString(x, y, 'Requested time: ')
		self.c.drawString(xt, y, str(report['Requested time']))
		self.c.drawString(xn, y, 'Threads per core: ' + report['Threads per core'])
		y += space
		self.c.drawString(x, y, 'Used time: ')
		self.c.drawString(xt, y, str(report['Used time']))
		y += space
		self.c.drawString(x, y, 'Time of job submission:')
		self.c.drawString(xt, y, str(report['Time of job submition']))
		y += space
		self.c.drawString(x, y, 'Time of job start:')
		self.c.drawString(xt, y, str(report['Time of job start']))
		y += space
		self.c.drawString(x, y, 'Time of completion: ')
		self.c.drawString(xt, y, str(report['Time of completion']))

		y += space
		################################
		nodelist=report['Node list'][0]
		nodes=[]
		nodes.append(report['Node list'][0])
		for node in report['Node list'][1:]:
			nodelist = nodelist + ', ' + node
			nodes.append(node)
		################################
		cmax=42
		nodecompactlist=NodeCompactList(cmax,nodes)
		ylast = y
		y += space
		#self.c.drawString(xn, y, 'Node list:')
		self.c.drawString(x, y, 'Node list:')
		for nodelist in nodecompactlist:
			#self.c.drawString(xn, y, nodelist)
			self.c.drawString(xt, y, nodelist)
			y += space
		y = ylast
		y += space
		################################


		#bild = 'seal.png'
		#c.drawImage(bild,350, 50, width=None, height=None)   # x = left side of image, you can resize with width and height
		image_path = tmpdir + '/' + 'global_barchart_' + str(jobid) + '.svg'
		scaling_factor = 1
		# position of image and text for image
		ixpos = 80
		iypos = 240
		txpos = 180
		typos = 460
		self.c.setFont('Helvetica', 14, leading=None)
		add_image(self.c,image_path, scaling_factor, ixpos, iypos, txpos, typos, 'Global Summary of Resource Usage')

		max_page_number = 1 + nptext + npbc + npbp + npts + npgs + 1

		# one more page for gpu description page
		if (npgs > 0):
			max_page_number = max_page_number + 2

		y = 200
		self.c.setFont('Helvetica',14,leading=None)
		self.c.drawString(x - 20, y, 'Recommendations:')   # text at this point (x,y from bottom)
		self.c.setFont('Helvetica', 10, leading=None)
		y = y - 30
		nl = len(rec_text)
		for itext in range(0,min(6,nl)):
			#print line
			self.c.drawString(x, y, rec_text[itext])
			y += space

		## place first recommendations in formatted paragraph
		#texttitle=
		#textlines=
		## set coordinates for paragraph
		#xpar=18
		#ypar=217
		#dyp=3
		##
		#styles = getSampleStyleSheet()
		## 'leading' sets line spacing
		#styles.add(ParagraphStyle(leading=10,name='Justify', alignment=TA_JUSTIFY,FontName='Helvetica',fontSize=9))
		#styletitle = ParagraphStyle(name='Normal',fontSize=10)
		#self.createParagraph(texttitle, xpar, 35 ,styles["Justify"])
		#p = Paragraph(textlines, styles["Justify"])
		#p.wrapOn(self.c, self.width-100, self.height)
		#p.drawOn(self.c, *self.coord(xpar, ypar+69, mm))

		#end page
		pfitPageFooter(self.c, page_number, max_page_number)
		self.c.showPage()  # completes page and starts a new page
		###########################################################################################################

		if (nptext > 0):
			for iptext in range(0,nptext):
				page_number = page_number + 1
				y = 700
				# print page header
				pfitPageHeader(self.c, date, jobid, logo_path)
				self.c.setFont('Helvetica',14,leading=None)
				self.c.drawString(x - 20, 740, 'Recommendations - continued:')
				self.c.setFont('Helvetica', 10, leading=None)
				for itext in range(itext+1,min(nl,6+(iptext+1)*nrecpp)):
					# print line
					print('itext ', itext)
					c.drawString(x, y, rec_text[itext])
					y += space

				pfitPageFooter(self.c, page_number, max_page_number)
				self.c.showPage()  # completes page and starts a new page

		###########################################################################################################
		####### node distributions as table and boxplots #######

		# create legend page
		page_number = page_number + 1
		# Kopfzeile
		pfitPageHeader(self.c, date, jobid, logo_path)
		# coordinates for title
		x = 100
		y = 730
		# title
		self.c.setFont('Helvetica', 14, leading=None)
		self.c.drawString(x + 135, y, 'Node Distributions')
		self.c.setFont('Helvetica', 9, leading=None)
		self.c.drawString(50, 35, '*Boxplot shows minimum, first quartile, median, third quartile, and maximum')
		self.c.setFont('Helvetica', 14, leading=None)

		datatab=[['Metric','Units','Min.','Max.','StdDev.','Avg.','Boxplot*']]
		for case in bp_cases:
			line=[]
			line.append(case['label'])
			#line.append('[' + case['units'] + ']')
			line.append(case['units'])
			for number in case['data']:
				#print("{0:.2f}".format(float(number)))
				line.append("{0:.2f}".format(float(number)))
			#line.extend(case['data'])
			#hier
			#image=Image(case['bpgraficfile'])
			image=scale(svg2rlg(case['bpgraficfile']),scaling_factor=0.28)
			line.append(image)
			datatab.append(line)
		#print(datatab)
		#t=Table(datatab,style=[('BACKGROUND',(1,1),(-2,-2),colors.green),('TEXTCOLOR',(0,0),(1,-1),colors.red)])
		t=Table(datatab,style=[('GRID',(0,0),(-1,-1),0.5,colors.black),('ALIGN',(0,0),(-1,-1),'CENTER'),('VALIGN',(0,0),(-1,-1),'MIDDLE'),('FONTSIZE',(0,0),(-1,-1),10)])
		# canvas,width,height
		wc=500
		hc=800
		t.wrapOn(self.c,wc,hc)
		# canvas,x,y
		# for scaling_factor=0.2 and 14 rows
		#xc=50
		#yc=200
		# for scaling_factor=0.25 and 14 rows
		#xc=50
		#yc=100
		# for scaling_factor=0.28 and 14 rows
		xc=50
		yc=50
		t.drawOn(self.c,xc,yc)

		# finish page
		pfitPageFooter(self.c, page_number, max_page_number)

		self.c.showPage()  # completes page and starts a new page


		####### end node distributions as table and boxplots #######
		###########################################################################################################

		# Bar chart pages
		# initialise general parameters
		# coordinates for title
		x = 100
		y = 730
		tdist = 25
		scaling_factor = 0.75

		# max number of bar chart coordinates
		#nc=len(coords)
		# number of bar charts per page
		nc=nbcpp
		if nc > 0:
			# index bar chart
			ibc=0
			# total number of bar charts
			nbc=len(bc_labels)

			# xxxxx
			ixpos=80
			iypos=550
			ncol=2
			#nrow=4
			nrow=math.ceil(nc/ncol)
			hx = 450
			hy = 650
			dx = hx/ncol
			dy = hy/nrow
			dxt = 70
			dyt = 130
			# program works for nbcpp between about 4 to 14
			if nbcpp > 10:
				scaling_factor = 0.45
				iypos = 600
				hy = 650
				dy = hy/nrow
				dyt = 65
			# calculate coordinates of charts (row,col)
			pos={}
			k=0
			for i in range(0,nrow):
				for j in range(0,ncol):
					px=ixpos + dx*j
					py=iypos - dy*i
					tx=ixpos + dx*j + dxt
					ty=iypos - dy*i + dyt
					pos.update({'px' + str(k):px})
					pos.update({'py' + str(k):py})
					pos.update({'tx' + str(k):tx})
					pos.update({'ty' + str(k):ty})
					k=k+1
			# xxxxxx

			# loop for plots
			# ip = index of barchart page; npbc = total number of pages with barcharts
			# page_number = pdf report page number
			for ip in range(1,npbc+1):
				page_number = page_number + 1
				# Page contents
				# Kopfzeile
				pfitPageHeader(self.c, date, jobid, logo_path)
				# Initialise
				self.c.setFont('Helvetica', 14, leading=None)

				#c.drawString(x - 20, y, 'Node Distributions:')   # text at this point (x,y from bottom)
				self.c.drawString(x + 135, y, 'Node Distributions')
				self.c.setFont('Helvetica', 10, leading=None)
				#c.drawString(x - 20, y-15, '(minimum, maximum, average, standard deviation)')
				#self.c.drawString(x + 88, y-15, '(minimum, maximum, average, standard deviation)')
				self.c.drawString(x + 88, y-15, '(Box plots: minimum, average, maximum, 50% boundaries)')
				self.c.setFont('Helvetica', 12, leading=None)

				# ic = index of bar chart coordinates
				# nc = total number of bar chart coordinates (per page)
				for ic in range(0,nc):
					if (ibc < nbc):
						#image_path = tmpdir + '/' + bc_files[ibc] + '_' + str(jobid) + '.svg'
						image_path = tmpdir + '/' + bp_files[ibc] + '_' + str(jobid) + '.svg'
						px=pos['px' + str(ic)]
						py=pos['py' + str(ic)]
						tx=pos['tx' + str(ic)]
						ty=pos['ty' + str(ic)]
						#add_image(c,image_path, scaling_factor, px, py, tx, ty, bc_labels[ibc])
						#print(ibc,bc_labels[ibc],bc_units[ibc])
						add_image_units(self.c,image_path, scaling_factor, px, py, tx, ty, bc_labels[ibc],bc_units[ibc])
						ic=ic+1
						ibc=ibc+1
					else:
						break

				pfitPageFooter(self.c, page_number, max_page_number)

				self.c.showPage()  # completes page and starts a new page

		###########################################################################################################
		# Nächste Seiten (Zeitverläufe)

		# initialise parameter for plot scaling
		scaling_factor = 0.6

		# determine coordinates for graphic file
		ixpos = 0
		iypos = 30
		# height of plot
		hp = 500
		# x position of plot labels
		xtext = 480
		dyt = hp/nsubpp
		ytext = iypos + hp + 150 - 0.5*dyt
		# total number of timeseries plots
		nts=len(ts_labels)

		# loop for timeseries pages

		for ip in range(0,npts):
			page_number = page_number + 1

			image_path = tmpdir + '/' + ts_files[ip] + '_' + str(jobid) + '.svg'
			add_image_only(self.c,image_path, scaling_factor, ixpos, iypos)

			#for its in range(ip*nsubpp,min((ip+1)*nsubpp,nts)):
			#	add_text(self.c, xtext, ytext, ts_labels[its])
			#	ytext = ytext - dyt

			# write titles last so it will be written over the image
			# Kopfzeile
			pfitPageHeader(self.c, date, jobid, logo_path)
			self.c.setFont('Helvetica', 14, leading=None)
			x = 70
			y = 720
			self.c.drawString(x + 180, y, 'Node Timeseries Plots')
			self.c.setFont('Helvetica', 10, leading=None)

			pfitPageFooter(self.c, page_number, max_page_number)

			self.c.showPage()  # completes page and starts a new page

		###########################################################################################################
		# create legend page
		page_number = page_number + 1
		# Kopfzeile
		pfitPageHeader(self.c, date, jobid, logo_path)

		texttitle = '<font size=10><b>Global Summary Definitions:<br /></b></font>'
		textlines = """
		The global summary bar chart gives information about how resources are being used or how far they are from a maximum value. Each
		bar represents the comparison of two values, which are listed to the right of the diagram. <br />
		<br />
		<u>Walltime:</u> The time duration between time of job start until time of job completion. It is also referred to as used time.
                This value is compared in the bar chart with the requested time.
		<br />
		<br />
		<u>CPU usage:</u> Average of the CPU usages (see glossary) of all job‘s processes on allocated nodes. It is compared in the bar chart
                to the value 100% or to the highest value, if larger that 100%.
		<br />
		<br />
		<u>Memory sum:</u> Sum of the maximum memories (RAM) used by the job on allocated nodes. It is compared in the graph to the sum of the
                total RAM memory of the allocated nodes.
		<br />
		<br />
		<u>Swap sum:</u> Measure of how much swap space on a disk had to be used because of full physical RAM memory.
		<br />
		<br />
		<u>Network:</u> Compares maximum node traffic (Infiniband), the maximum amount received and the maximum amount transmitted per node.
		<br />
		<br />
		<u>I/O</u> (Input/Output): Compares the amount of read input to the amount of write output.
		<br />
		<br />
		<font size=10><b>Node Distribution Definitions:</b></font>
		<br />
		Bar graphs containing node distribution information. Overall values are computed for each node. Then the
		minimum, maximum, average and standard deviation of these values are computed.
		<br />
		<br />
		<u>CPU usage:</u> Time average of the sum of CPU usages (see glossary) of all job‘s processes on each node divided by the
		number of cores on that node.
		<br />
		<br />
		<u>Memory MaxRSS:</u> Maximum of the sum of the RSS (see glossary) values of each process of the job. It refers to
		the maximum amount of physical memory held by a processes for the job.
		<br />
		<br />
		<u>Memory AveRSS:</u> Average of the sum of the RSS  (see glossary)  values of each process of the job.
		<br />
		<br />
		<u>Memory Swap:</u> Indicates how much disk space was needed for execution of any processes of the job.
		<br />
		<br />
		<u>I/O Read or Write Size:</u> Amount of input read  or output written by job‘s processes on the node.
		<br />
		<br />
		<u>I/O Read or Write Count:</u> Count of number of read or write requests of job‘s processes on the node.
		<br />
		<br />
		<u>Max IB receive rate:</u> maximum (high water mark) of infiniband receiving rate of a node.
		<br />
		<br />
		<u>Max IB tranfer rate:</u> maximum (high water mark) of infiniband transmitting rate of a node.
		<br />
		<br />
		<u>CPU time:</u> <u>-user</u> Sum of amount of time CPU was executing user instructions for the job‘s processes.
		<u>-system</u> Sum of amount of time CPU was executing system instructions for the job‘s processes.
		<u>-idle</u> Sum of amount of time CPU was idle while executing job‘s processes.
		<u>-iowait</u> Sum of amount of time CPU was waiting for I/O for the job‘s processes.
		<br />
		<br />
		<font size=10><b>Timeseries:</b></font>
		<br />
		<u>Node CPU usage:</u> Sum of CPU usages of all cores on each allocated node divided by the number of cores allocated
		on that node for the job.
		<br />
		<br />
		<u>Node Load1:</u> Average values of load of the last minute of the node; see also glossary.
		<br />
		<br />
		<u>CPU usage:</u> Average of CPU usages of all job‘s processes on the node.
		<br />
		<br />
		<u>Memory RSS:</u> Sum of the job‘s process RSS (see glossary) on the node.
		<br />
		<br />
		<u>Sum Read Counts:</u> Sum of the job‘s read requests on the node.
		<br />
		<br />
		<u>Sum Write Counts:</u> Sum of the job‘s Write requests on the node.
		<br />
		<br />
		<u>IB max. receive rate:</u> Maximum infiniband receiving rate during job on the node.
		<br />
		<br />
		<u>IB max. transmit rate:</u> Maximum infiniband transmitting rate during job on the node.
		<br />
		<br />
		<font size=10><b>Glossary:</b></font>
		<br />
		<u>CPU:</u> Central Processing Unit
		<br />
		<u>CPU usage:</u> Percentage of time for a given time increment, for which a CPU was utilized by a process or vice versa
		<br />
		<u>Load1:</u> Load average for last 1 minute; a measure of how processor cores are being used in counts, where 1
		refers to full use of one core
		<br />
		<u>RAM:</u> Random Access Memory; physical memory
		<br />
		<u>RSS:</u> The Resident Set Size; the amount of physical memory (RAM) held by a process
		<br />
		<u>Swap:</u>  How much disk space was utilized during process execution instead of RAM
		"""

		# set coordinates for paragraph
		xpar=18
		ypar=217
		dyp=3
		#
		styles = getSampleStyleSheet()
		# 'leading' sets line spacing
		styles.add(ParagraphStyle(leading=10,name='Justify', alignment=TA_JUSTIFY,FontName='Helvetica',fontSize=9))
		styletitle = ParagraphStyle(name='Normal',fontSize=10)
		self.createParagraph(texttitle, xpar, 35 ,styles["Justify"])
		p = Paragraph(textlines, styles["Justify"])
		p.wrapOn(self.c, self.width-100, self.height)
		p.drawOn(self.c, *self.coord(xpar, ypar+69, mm))


		# write title last
		self.c.setFont('Helvetica', 12, leading=None)
		x = 125
		y = 755
		self.c.drawString(x + 160, y, 'Definition of Variables')
		self.c.setFont('Helvetica', 10, leading=None)

		pfitPageFooter(self.c, page_number, max_page_number)

		self.c.showPage()  # completes page and starts a new page
		###########################################################################################################



		# create gpu description page
		if (nsubpg > 0):
			page_number = page_number + 1
			# Kopfzeile
			pfitPageHeader(self.c, date, jobid, logo_path)

			####### GPU distributions as table and boxplots #######

			# coordinates for title
			x = 100
			y = 730
			# title
			self.c.setFont('Helvetica', 14, leading=None)
			self.c.drawString(x + 135, y, 'GPU Distribution Summary')
			self.c.setFont('Helvetica', 9, leading=None)
			self.c.drawString(50, 35, '*Boxplot shows minimum, first quartile, median, third quartile, and maximum')
			self.c.setFont('Helvetica', 14, leading=None)

			datatab=[['Metric','Units','Min.','Max.','StdDev.','Avg.','Boxplot*']]
			for case in gp_cases:
				line=[]
				line.append(case['label'])
				line.append(case['units'])
				for number in case['data']:
					line.append("{0:.2f}".format(float(number)))
				image=scale(svg2rlg(case['bpgraficfile']),scaling_factor=0.28)
				line.append(image)
				datatab.append(line)
			t=Table(datatab,style=[('GRID',(0,0),(-1,-1),0.5,colors.black),('ALIGN',(0,0),(-1,-1),'CENTER'),('VALIGN',(0,0),(-1,-1),'MIDDLE'),('FONTSIZE',(0,0),(-1,-1),10)])
			# canvas,width,height
			wc=500
			hc=800
			t.wrapOn(self.c,wc,hc)
			# canvas,x,y
			# for scaling_factor=0.2 and 14 rows
			xc=50
			yc=200
			# for scaling_factor=0.25 and 14 rows
			#xc=50
			#yc=100
			# for scaling_factor=0.28 and 14 rows
			#xc=50
			#yc=40
			t.drawOn(self.c,xc,yc)

			####### end node distributions as table and boxplots #######


			# set coordinates for paragraph
			xpar=18
			ypar=217
			dyp=3
			#
			styles = getSampleStyleSheet()
			# 'leading' sets line spacing
			styles.add(ParagraphStyle(leading=10,name='Justify', alignment=TA_JUSTIFY,FontName='Helvetica',fontSize=9))
			styletitle = ParagraphStyle(name='Normal',fontSize=10)
			#self.createParagraph(texttitle, xpar, 40 ,styles["Justify"])
			#p = Paragraph(textlines, styles["Justify"])
			#p.wrapOn(self.c, self.width-100, self.height)
			#p.drawOn(self.c, *self.coord(xpar, ypar+69, mm))


			# write title last
			#self.c.setFont('Helvetica', 12, leading=None)
			#x = 125
			#y = 755
			#self.c.drawString(x + 160, y, 'GPU Variables')
			self.c.setFont('Helvetica', 10, leading=None)

			pfitPageFooter(self.c, page_number, max_page_number)

			self.c.showPage()  # completes page and starts a new page

		###########################################################################################################
		# Nächste Seiten (GPU-Zeitverläufe)
		if (nsubpg > 0):

			# initialise parameter for plot scaling
			scaling_factor = 0.6

			# determine coordinates for graphic file
			ixpos = 0
			iypos = 30
			# height of plot
			hp = 500
			# x position of plot labels
			xtext = 480
			dyt = hp/nsubpg
			ytext = iypos + hp + 150 - 0.5*dyt
			# total number of timeseries plots
			ngts=len(gts_labels)

			# loop for timeseries pages

			for ip in range(0,npgs):
				page_number = page_number + 1

				image_path = tmpdir + '/' + gts_files[ip] + '_' + str(jobid) + '.svg'
				add_image_only(self.c,image_path, scaling_factor, ixpos, iypos)

				# write titles last so it will be written over the image
				# Kopfzeile
				pfitPageHeader(self.c, date, jobid, logo_path)
				self.c.setFont('Helvetica', 14, leading=None)
				x = 70
				y = 740
				self.c.drawString(x + 180, y, 'GPU Timeseries Plots')
				self.c.setFont('Helvetica', 10, leading=None)

				pfitPageFooter(self.c, page_number, max_page_number)

				self.c.showPage()  # completes page and starts a new page

		###########################################################################################################


		# create gpu description page
		if (nsubpg > 0):
			page_number = page_number + 1
			# Kopfzeile
			pfitPageHeader(self.c, date, jobid, logo_path)

			texttitle = '<font size=10><b>Global Summary Definitions:<br /></b></font>'
			textlines = """
			Whenever GPUs are available, two additional global values are included on the first page of the report in
			the global summary diagram.
			<br />
			<br />
			<u>GPU-usage:</u> Average GPU usage of all GPUs of allocated nodes for the job in %
			<br />
			<br />
			<u>GPU Memory sum:</u> Sum of the maximum memories used by the job on GPUs of the allocated nodes. It is compared
			in the graph to the sum of the total memory of GPUs of the allocated nodes. Units are in MB.
			<br />
			<br />
			<br />
			<font size=10><b>GPU Metrics:</b></font>
			<br />
			<br />
			The following metrics are gathered and aggregated from each GPU. Notice that HWM stands for High Water Mark.<br />
			<br />
			<b>GPU Distribution Definitions:</b>
			<br />
			<br />
			<u>Bus:</u> BUS ID of the GPU
			<br />
			<br />
			<u>Power Limit:</u> Power limit of GPU in Watts
			<br />
			<br />
			<u>Total Memory:</u> The total memory in MB
			<br />
			<br />
			<u>Used Memory HWM:</u> Used memory in MB. HWM (high water mark)
			<br />
			<br />
			<u>Temperature HWM:</u> GPU temperature in C. HWM
			<br />
			<br />
			<u>Power HWM:</u> HWM of the power used in Watts
			<br />
			<br />
			<u>GPU Usage HWM:</u> HWM of the GPU utilization
			<br />
			<br />
			<u>GPU Usage Avg.:</u> Mean of the GPU utilization
			<br />
			<br />
			<u>CPU Usage HWM:</u> HWM of CPU usage of processes using the GPU
			<br />
			<br />
			<u>Memory RSS HWM:</u> HWM of RSS memory of processes using the GPU
			<br />
			<br />
			<u>No. GPU Processes:</u> Total number of unique processes which used the GPU
			<br />
			<br />
			<br />
			<b>GPU Timeseries Metrics Definitions:</b>
			<br />
			<br />
			<u>Memory RSS Sum:</u> The sum of RSS (Resident Set Size) memory consumed by all CPU processes on a GPU in period delta.
			<br />
			<br />
			<u>No. GPU Processes:</u> Number of CPU processes using a GPU in period delta (total amount per interval).
			<br />
			<br />
			<u>Sum CPU Usage:</u> The sum of all CPU usages of all processes on the GPU in period delta.
			<br />
			<br />
			<u>GPU Memory Usage:</u> Time series of GPU memory usage.
			<br />
			<br />
			<u>Power Consumption:</u>  Time series of GPU power consumption.
			<br />
			<br />
			<u>GPU Temperature</u> Time series of GPU temperatur in Celsius.
			<br />
			<br />
			<u>GPU usage:</u> Time series of GPU utilization in percent, maximum value is 100.
			<br />
			<br />
			<br />
			<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
			<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
			<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
			<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
			<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
			<br /> <br /> <br /> <br /> <br /> <br />
			"""


			# set coordinates for paragraph
			xpar=18
			ypar=350
			dyp=3
			#
			styles = getSampleStyleSheet()
			# 'leading' sets line spacing
			styles.add(ParagraphStyle(leading=10,name='Justify', alignment=TA_JUSTIFY,FontName='Helvetica',fontSize=9))
			styletitle = ParagraphStyle(name='Normal',fontSize=10)
			self.createParagraph(texttitle, xpar, 40 ,styles["Justify"])
			p = Paragraph(textlines, styles["Justify"])
			p.wrapOn(self.c, self.width-100, self.height)
			p.drawOn(self.c, *self.coord(xpar, ypar+69, mm))


			# write title last
			self.c.setFont('Helvetica', 12, leading=None)
			x = 125
			y = 755
			self.c.drawString(x + 100, y, 'Definition of GPU Variables')
			self.c.setFont('Helvetica', 10, leading=None)

			pfitPageFooter(self.c, page_number, max_page_number)

			self.c.showPage()  # completes page and starts a new page

		###########################################################################################################



##### xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

def PickTS(conf):
	tsconf=[]
	for line in conf:
		if(line.startswith("timeseries")):
			line=line.strip()
			tsconf.append(line)
	return tsconf
def PickGTS(conf):
	tsconf=[]
	for line in conf:
		if(line.startswith("gputs")):
			line=line.strip()
			tsconf.append(line)
	return tsconf
def GetTSnplot(conf):
	nsubpp = 4
	for line in conf:
		if(line.startswith("nsubpp")):
			line=line.strip()
			type,val=line.split(";")
			nsubpp=int(val)
	return nsubpp
def GetGTSnplot(conf):
	nsubpg = 7
	for line in conf:
		if(line.startswith("nsubpg")):
			line=line.strip()
			type,val=line.split(";")
			nsubpg=int(val)
	return nsubpg

def main(argv):

	NoneType=type(None)
	print('reading ' + argv[0])

	with open(argv[0],'r') as f:
		agg_dict = json.load(f)
	f.close()

	tmpdir = argv[1]
	pdfdir = argv[2]

	conffile = os.path.dirname(os.path.abspath(__file__)) + '/../conf/pdfreport.conf'
	use_conffile = os.path.isfile(conffile)
	if use_conffile:
		print('use configuration file:',conffile)
		f=open(conffile)
		conf=f.readlines()
		f.close()
	else:
		print('use defaults')
		conf=[]


	#extract job data
	requested_time=agg_dict['job']['requested_time']
	start_time=agg_dict['job']['start_time']
	user_name=agg_dict['job']['user_name']
	num_nodes=agg_dict['job']['num_nodes']
	used_queue=agg_dict['job']['used_queue']
	job_id=agg_dict['job']['job_id']
	used_time=agg_dict['job']['used_time']
	submit_time=agg_dict['job']['submit_time']
	requested_cu=agg_dict['job']['requested_cu']
	end_time=agg_dict['job']['end_time']

	###### plot global results in barchart
	data = GetGlobalData(agg_dict)


	print('creating global barchart for JobID',job_id)
	hbar_global(data, job_id, tmpdir)


	##### Variable barchart and boxplot diagrams of general data ######
	# define cases in ordered lists
	# barnames = list of variables to be plotted
	# barunits = list of corresponding units
	# bc_labels = list of corresponding labels (captions)
	# bc_files = list of corresponding file identification strings
	##### Variable boxplot diagrams ######
	# bp_labels = list of corresponding labels (captions)
	# bp_files = list of corresponding file identification strings
	# case = local dictionary containing single case information

	create_hbar=0

	barnames,barunits,bc_labels = GetBarVars(conf,agg_dict['nodes'])
	bc_files=[]
	bc_units=[]
	bp_files=[]
	bp_units=[]
	bp_cases=[]
	# iterate cases (new_plot_stat: evaluate maximum, minimum and average values)
	for i in range(0,len(barnames)):
		case = node_statistics(barnames[i], bc_labels[i], job_id, agg_dict['nodes'], barunits[i], tmpdir)
		bc_files.append(case['fileid'])
		bc_units.append(case['units'])
		bp_files.append(case['bpfileid'])
		bp_units.append(case['units'])
		#print('creating barchart',barnames[i])
		if create_hbar:
			hbar_var(case)
		boxplot(case)
		bp_cases.append(case)


	##### Timeseries diagrams for nodes ######
	# tsnames = list of variables to be plotted
	# tsunits = list of corresponding units
	# ts_labels = list of corresponding captions
	# ts_files = list of corresponding file identification strings

	tsnames = []
	tsunits = []
	ts_labels = []
	tsconf = PickTS(conf)
        # use sorted list of keys, so sequence will always be the same
	for key in sorted(agg_dict['nodes'][0]['dynamic']):
		if type(agg_dict['nodes'][0]['dynamic'][key]['data']) != NoneType:
			tsnames.append(key)
			tsunit,tslabel=GetUnits(tsconf,key)
			tsunits.append(tsunit)
			ts_labels.append(tslabel)

	start_time_loc=time.strftime("%Y-%m-%dT%H:%M:%S",time.localtime(start_time))
	end_time_loc=time.strftime("%Y-%m-%dT%H:%M:%S",time.localtime(end_time))

	ts_files = []
	tag = 'host:'
	# nsubpp = number of subplots per plot
	# nsubpp = 6
	nsubpp = GetTSnplot(conf)
	# index of subplot
	#ndf=0
	# number of plots
	nplots = math.ceil(len(tsnames)/nsubpp)
	dfs = {}
	udfs = {}
	ldfs = {}
	for ip in range(0,nplots):
		dfnames=[]
		for i in range(ip*nsubpp,min((ip+1)*nsubpp,len(tsnames))):

			data_array,data_col,conversion = GetDynamicDataArray(agg_dict['nodes'],tsnames[i])
			periods = len(agg_dict['nodes'][0]['dynamic'][tsnames[i]]['data'])
			data_index=pd.date_range(start=start_time_loc, end=end_time_loc, periods=periods)
			pddata = pd.DataFrame(data_array,index=data_index,columns=data_col)
			if (tsnames[i] == 'proc_cpu_usage'):
				pddata = SpecialConversion(pddata,conversion)
			pddata,units = ConvertUnitsDF(pddata,tsunits[i])
			dfs.update({'pddata' + str(i):pddata})
			udfs.update({'pddata' + str(i):units})
			ldfs.update({'pddata' + str(i):ts_labels[i]})
			dfnames.append('pddata' + str(i))

		fileid = 'ts_' + str(ip)
		ts_files.append(fileid)
		graficfile = tmpdir + '/' + fileid + '_' + str(job_id) + '.svg'

		timeseries_varsp(nsubpp,dfnames, udfs, ldfs, tag, dfs, graficfile)

	##### Timeseries diagrams for gpus ######
	# gtsnames = list of variables to be plotted
	# gtsunits = list of corresponding units
	# gts_labels = list of corresponding captions
	# gts_files = list of corresponding file identification strings
	nsubpg = 0
	if 'gpus' in agg_dict['nodes'][0] and agg_dict['nodes'][0]['gpus'] is not None:

		##### Variable boxplot diagrams of gpu data ######
		# define cases in ordered lists
		# gpunames = list of variables to be plotted
		# gpuunits = list of corresponding units
		# gp_labels = list of corresponding labels (captions)
		# gp_files = list of corresponding file identification strings
		# case = local dictionary containing single case information

		gpunames,gpuunits,gp_labels = GetGpuVars(conf,agg_dict['nodes'])
		gp_files=[]
		gp_units=[]
		gp_cases=[]
		# iterate cases (new_plot_stat: evaluate maximum, minimum and average values)
		for i in range(0,len(gpunames)):
			case = gpu_statistics(gpunames[i], gp_labels[i], job_id, agg_dict['nodes'], gpuunits[i], tmpdir)
			gp_files.append(case['bpfileid'])
			gp_units.append(case['units'])
			boxplot(case)
			gp_cases.append(case)

		############

		gtsnames = []
		gtsunits = []
		gts_labels = []
		gts_files=[]
		gtsconf = PickGTS(conf)
        	# use sorted list of keys, so sequence will always be the same
		#for n in range(0,num_nodes):
		for key in sorted(agg_dict['nodes'][0]['gpus'][0]['dynamic']):
			if type(agg_dict['nodes'][0]['gpus'][0]['dynamic'][key]['data']) != NoneType:
				gtsnames.append(key)
				gtsunit,gtslabel=GetUnits(gtsconf,key)
				gtsunits.append(gtsunit)
				gts_labels.append(gtslabel)

		start_time_loc=time.strftime("%Y-%m-%dT%H:%M:%S",time.localtime(start_time))
		end_time_loc=time.strftime("%Y-%m-%dT%H:%M:%S",time.localtime(end_time))

		tag = 'host-gpu(bus):'
		# nsubpg = number of subplots per gpu plot
		# nsubpg = 6
		nsubpg = GetGTSnplot(conf)
		if (len(gts_labels) < nsubpg):
			nsubpg=len(gts_labels)
		# index of subplot
		#ndf=0
		# number of plots
		nplots = math.ceil(len(gtsnames)/nsubpg)
		dfs = {}
		udfs = {}
		ldfs = {}
		for ip in range(0,nplots):
			dfnames=[]
			for i in range(ip*nsubpg,min((ip+1)*nsubpg,len(gtsnames))):

				data_array,data_col,conversion = GetDynamicGpuDataArray(agg_dict['nodes'],gtsnames[i])
				periods = len(agg_dict['nodes'][0]['gpus'][0]['dynamic'][gtsnames[i]]['data'])
				data_index=pd.date_range(start=start_time_loc, end=end_time_loc, periods=periods)
				pddata = pd.DataFrame(data_array,index=data_index,columns=data_col)
				if (gtsnames[i] == 'proc_cpu_usage'):
					pddata = SpecialConversion(pddata,conversion)
				pddata,units = ConvertUnitsDF(pddata,gtsunits[i])
				dfs.update({'pddata' + str(i):pddata})
				udfs.update({'pddata' + str(i):units})
				ldfs.update({'pddata' + str(i):gts_labels[i]})
				dfnames.append('pddata' + str(i))

			fileid = 'gts_' + str(ip)
			gts_files.append(fileid)
			graficfile = tmpdir + '/' + fileid + '_' + str(job_id) + '.svg'

			timeseries_gpu(nsubpg,dfnames, udfs, ldfs, tag, dfs, graficfile)
	else:
		gts_labels=[]
		gts_files=[]
		gp_files=[]
		gp_units=[]
		gp_cases=[]
		gp_labels=[]


	if nsubpg > 0:
		###### plot global gpu results in barchart
		data = GetGlobalGPUData(agg_dict)


		print('creating global GPU barchart for JobID',job_id)
		hbar_global_gpu(data, job_id, tmpdir)

	# report = dictionary containing text for report
	# create pdf file
	report = get_report(agg_dict)
	pages = ReportMaker(report,tmpdir,pdfdir,bc_units,bc_labels,bc_files,bp_files,bp_cases,ts_labels,ts_files,nsubpp,gts_labels,gts_files,nsubpg,gp_units,gp_labels,gp_files,gp_cases)
	pages.make_formatted_report()
	pages.savePDF()

	return 0

if __name__ == "__main__":
	if len(sys.argv[1:]) < 3:
		print('three parameters required for',sys.argv[0])
		print('json-file tmp-directory pdf-report-directory')
		print('exiting')
	else:
		main(sys.argv[1:])
