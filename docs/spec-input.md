# Input specification

Here is the input specification for PDF report.

    {
        "job": {
                "job_id": STR,
                "user_name": STR,
                "used_queue": STR,
                "submit_time": INT,
                "start_time": INT,
                "end_time": INT,
                "used_time": INT,
                "requested_time": INT,
                "requested_cu": INT,
                "num_nodes": INT,
                "recommendations": STR,
        },

        "nodes": [
              {
                "node_name": STR,
                "cpu_time_user": INT,
                "cpu_time_system": INT,
                "cpu_time_idle": INT,
                "cpu_time_iowait": INT,
                "write_bytes": INT,
                "write_count": INT,
                "read_bytes": INT,
                "read_count": INT,
                "mem_rss_max": INT,
                "mem_rss_avg": INT,
                "mem_swap_max": INT,
                "mem_vms": INT,
                "cpu_usage": NUM,
                "cpu_model": STR,
                "sockets": INT,
                "cores_per_socket": INT,
                "phys_threads_per_core": INT,
                "virt_threads_per_core": INT,
                "available_main_mem": INT,
                "num_cores": INT,

                "dynamic": {
                  "node_cpu_usage": {
                    "delta": INT,
                    "data": [NUM,]
                    },
                  "node_load": {
                    "delta": INT,
                    "data": [NUM,]
                    },
                  "proc_cpu_usage": {
                    "delta": INT,
                    "data": [NUM,]
                    },
                  "proc_mem_rss_sum": {
                    "delta": INT,
                    "data": [INT,]
                    },
                },
              }
        ],
    }

## job

Contains information about the job.

|  name           | type                    | comment                          |
|  -------------  | ----------------------- | -------------------------------- |
|  `job_id`       | String, valid JOBID     | Job's ID                         |
|  `user_name`    | String                  | username of the jobstarter       |
|  `used_queue`   | String                  | Queue in which job was submitted |
|  `submit_time`  | Integer, Unix timestamp | Submission date                  |
|  ...            | ...                     | ...                              |
|                 |                         |                                  |
|`recommendations`| List of strings from <br>  aggregator and <br> recommendation system  | ["string(1)","string(2)","string(3)",...,"string(n)"] where string(i) <br> corresponds to a line of text and has maximum of <br>  98 characters; notice that this variable is not required |

## nodes

Contains data aggregated by node.

| name                 | type             | comment                                    |
| -------------------- | ---------------- | ------------------------------------------ |
| `node_name`          | String           | Node name                                  |
| `cpu_time_user`      | Integer, Seconds | Amount of time CPU spent on user processes |
| `available_main_mem` | Integer, Bytes   | The memory size of each node in bytes      |
| ...                  | ...              | ...                                        |

## nodes.dynamic

Contains timeseries data aggregated by node.

Measurements start at time `job.start_time` and `delta` is the difference between timestamps of 2 consecutive Measurements. `data` is a list which consists of consecutive measurements.

If measurement does not exist then there should stay `None` as placeholder.

TODO: agree on maximum amount of measurements.

| name               | type           | comment                                                                |
| ------------------ | -------------- | ---------------------------------------------------------------------- |
| `node_cpu_usage`   | Float          | Average CPU usage of the node in percent.                              |
| `node_load`        | Float          | Average values of load of the last minute (load1) of the node          |
| `proc_cpu_usage`   | Float          | Average of the SUM of CPU usages of all job's processes on the node    |
| `proc_mem_rss_sum` | Integer, Bytes | The sum of job's process RSS on the node                               |
