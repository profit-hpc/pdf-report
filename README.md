# pdf-report

### PDF report

Program to create a PDF-Report from data supplied for a job run on an HPC-system

### Dependencies:

pdfreport.py was developed on an Ubuntu16.04-System and has the following
dependencies:

Python (>= 3.5)

numpy(>= 1.15.4)

matplotlib (>= 3.0.2)

pandas (>= 0.23.4)

reportlab (>= 3.3.0)

svglib (== 0.9.0)


 ### Installation notes:

 After installation of all dependencies, pdfreport.py can be called with the
 line command:

 python3 \<pdf-report-home\>/pdfgen/pfit_pdfreport.py \<name-of-json-file\> \<tmp-output-dir\> \<pdf-report-dir\>

 where
 
 \<pdf-report-home\> is the directory of this file

 \<name-of-json-file\> is the path to the json input file containing the job-ID-data
 (see docs/spec-input)

 \<tmp-output-dir\> is the directory for temperary ouput files

 \<pdf-report-dir\> is the directory for the output pdf-report

A configuration file can be created to customize some of the graphs. If it
exists, it will be used. Otherwise default values will be used. It must have
the following path:

\<pdf-report-home\>/conf/pdfreport.conf


 ### Test:
 
 Create a directory for temporary output files and for output PDF report.
 In this example, the PDF report will be place in the local directory
 
 mkdir ./tmp

 python3 \<pdf-report-home\>/pdfgen/pfit_pdfreport.py \<pdf-report-home\>/testdata/test-ib_1352076.json ./tmp ./

 ### Current defined data and structure

 See docs/spec-input. Missing data may either remain undefined in the json file or defined with value Null. 
 Json data type Null converts to data type None in python.

 ### Extending pdfgen/pfit_pdfreport.py to include data not mentioned in docs/spec-input

 All timeseries data will be plotted, if they are included properly in the  
 nodes.dynamic section in docs/spec-input, for example, similar to `node_load`.
 For more details on displaying other data, see comments in pdfreport.py and
 follow instructions for modifying the file conf/pdfreport.conf.

 ### Author:

 Rosemarie Meuer, University of Rostock

 ### Date: January 16, 2020
